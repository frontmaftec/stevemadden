
//funciones de swiper para inicializar cada slide  
//Se agrega una clase para identificar el slide se usa el home* detail-product* 404* cart*

$(document).ready(function() {

   var swiper = new Swiper('.swhome', {
       pagination: '.swiper-pagination',
       slidesPerView: 1, //numero de imagenes visibles 
       paginationClickable: true, //para mostrar flechas
       nextButton: '.swiper-button-next', //clases para siguiente
       prevButton: '.swiper-button-prev'  //clases para previo
   
                                       });


//Comparten mismas propiedades las siguientes   

    var swiper = new Swiper('.swall', {
        pagination: '.swiper-pagination',
        slidesPerView: 4,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'

                                       });


    var swiper = new Swiper('.scart', { 
        pagination: '.swiper-pagination',
        slidesPerView: 4,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'

                                       });



    var swiper = new Swiper('.sslide', { 
        pagination: '.swiper-pagination',
        slidesPerView: 1,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'

                                       });


							               });