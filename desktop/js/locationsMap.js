// funcion para localizar en google maps
function initializeMap(change) {

    var divGetHtml = $(change).attr("data-windowTarget");  //Obtiene numero de div para obtener html
    latitudLocation = $(change).attr("data-lat");  // Obtiene Latitud
    longitudLocation = $(change).attr("data-long");  // Obtiene Longitud 

    latitudLocation = latitudLocation * 1; // Se arregla la posicion 
    longitudLocation = longitudLocation * 1;

    var map = new google.maps.Map(document.getElementById("map"), {  //Inicia el Mapa 
        zoom: 16, //El zoom que necesitamos colocar entre sea mayor mas cerca mostrata la ubicacion 
        center: {  // Donde centrara el mapa 
            lat: latitudLocation,
            lng: longitudLocation
        }
    }); 

    var contentString = $("div.infoWindowMap.target" + divGetHtml + "").html(); // Obtiene HTML 

    var infowindow = new google.maps.InfoWindow({
        content: contentString  //Le asigna el HTML para facilidad de uso todo se agrega en locations.php 
    });

    var beachMarker = new google.maps.Marker({  // Agrega el marcador a la posicion seleccionada 
        position: {
            lat: latitudLocation,
            lng: longitudLocation
        },
        map: map,
    });

    infowindow.open(map, beachMarker);  //Abre la ventana del Mapa con el html de informacion de locations.php 

}



// genera un retraso de 1 segundo para mostrar el mapa  

$(document).ready(function() {

    var counterMap = 0;

    setTimeout(function() {

        counterMap += 1;
        if (counterMap == 1) {

            $("#first-map").trigger("click");

        }
    }, 1000)

});