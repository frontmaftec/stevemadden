
var mapMenu_gbl = mapMenu_gbl || function(visibleMenuOptions, optionsDivMenu){
  
  var hideTimeOne,  
    hideTimeTwo, 
    $visibleMenuOptions = $(visibleMenuOptions), // esta es la clase de los li visibles en las opciones del menu
    $optionsDivMenu = $(optionsDivMenu)  // son los contenedores de las opciones del menu cuando abre o divs

  $visibleMenuOptions.mouseleave(function() {  //Activa el tiempo de retraso si el mouse abandona el elemento li 
    hideTimeOne = makeTimeOut();   
  });

  $optionsDivMenu.mouseleave(function() {  //Activa el tiempo de retraso si el mouse abandona el elemento div 
    hideTimeTwo = makeTimeOut();
  });

  function makeTimeOut(){   // Aplica el tiempo de retraso antes de ocultar el contendor div de opciones 
    return setTimeout(function(){ 
      hideMenu();
    }, 400);
  } 
 
  function hideMenu(){ 
    $optionsDivMenu.css('display', 'none');
    $visibleMenuOptions.removeClass('active');
  }

  $visibleMenuOptions.mouseover(function() {

    var $currentLink = $(this),
      targetDiv = $currentLink.attr('data-divtargetmenu'), // le decimos donde obtener el atributo personalizado del div que se quiere mostrar 
      $targetDiv = $('.'+targetDiv), //tenemos el atributo en la variable 
      screenWidth = $(document).width(),  // obtenemos el ancho del documento 
      countColumns = $targetDiv.children().length,  //cuenta el numero de columnas hijo para calcular el ancho esto automatiza el calculo de columnas
      optionsWidth, 
      numCol1 = 10,  //Asignamos el ancho por defecto de cada columna en porcentaje 
      numCol2 = 40,
      numCol3 = 70,
      numCol4 = 100,
      newPosition,
      centerPixel, 
      pixelPosition,
      newWidth;

    hideMenu();  // Oculta de forma inmediata todos los demas div activos o bien solo pegar las dos lineas de la funcion hide 

    $targetDiv.css('display', 'block');  // Muestra el div objetivo 

    optionsWidth = $currentLink.position().left + $currentLink.outerWidth() / 2;  // Calcula el centro 

    if(screenWidth > 2300) {  // Agregamos las resoluciones de pantalla para ajustar dinamicamente el menú en este caso 2300 px
      numCol1 = 10; // Cada variable tiene un numero en porcentaje para asignarselo al su contenedor se maneja % por ser mas facil de medir su impacto 
      numCol2 = 25; 
      numCol3 = 50;
      numCol4 = 60;
    }

/*Podemos agregar tantas resoluciones como queramos */ 

    if(screenWidth < 1280) {  // Medimos la pantalla para asignar otros % a los div 
      numCol1 = 10;
      numCol2 = 45;
      numCol3 = 100;
      numCol4 = 100;
    }




/* Desde aqui asigna nuevos valores dependiendo de la resolucion de la pantalla */ 

    if(countColumns == 1) { 
      centerPixel = screenWidth * numCol1 / 100 / 2;  // Encuentra el centro de la pantalla conforme al ancho del elemento a mostrar 
      newPosition = optionsWidth - centerPixel; // Centra el div con el centro del li 
      pixelPosition = optionsWidth + centerPixel; 
      newWidth = numCol1; 
    }

    if(countColumns == 2) {
      centerPixel = screenWidth * numCol2 / 100 / 2;
      newPosition = optionsWidth - centerPixel;
      pixelPosition = optionsWidth + centerPixel;
      newWidth = numCol2;
    }

    if(countColumns == 3) {
      centerPixel = screenWidth * numCol3 / 100 / 2;
      newPosition = optionsWidth - centerPixel;
      pixelPosition = optionsWidth + centerPixel;
      newWidth = numCol3;
    }

    if(countColumns == 4) {
      centerPixel = screenWidth * numCol4 / 100 / 2;
      newPosition = optionsWidth - centerPixel;
      pixelPosition = optionsWidth + centerPixel;
      newWidth = numCol4;
    }

/*Termina asignacion de valores se aceptan hasta 4 columas si queremos agregar otra solo copiamos un if y declaramos el comportamiento de columna 5 */ 

    if(pixelPosition > screenWidth) {
      newPosition -= (pixelPosition - screenWidth);  // Evita scroll y pega la caja de texto a la derecha 
    }

    if(newPosition < 0) {
      newPosition = 0;  //Evita el scroll y pega la caja de texto a la izquierda aunque no quede alineada en el centro del elemento li padre 
    }

    newWidth *= screenWidth / 100; //Asigna un valor en px 

    $targetDiv
      .css('width', newWidth+'px')  //termina asignando el ancho despues de calcular ancho de pantalla columnas 
      .css('margin-left',newPosition-1+"px"); //Muestra el centro exacto con error de 1 px para evitar en dado caso scroll 

    $currentLink.addClass('active');  // lo marca activo para colocar un color azul 

    clearTimeout(hideTimeOne); // Limpia el tiempo de abandono de li 
    clearTimeout(hideTimeTwo); // Limpia el tiempo de abandono de div

  });

  $optionsDivMenu.mouseover(function() { 
    clearTimeout(hideTimeOne);  // Limpia el tiempo de abandono de li 
    clearTimeout(hideTimeTwo);  // Limpia el tiempo de abandono de div
  });

};

mapMenu_gbl('.optionsMenu','.menuContent'); // podemos asignar nuevas clases cambiando estos valores tanto en el html y en este js 
