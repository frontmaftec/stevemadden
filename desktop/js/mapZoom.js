/**   
 * Funcion principal para realizar zoom a una imagen
 * @param  {string: selector} parent    El contenedor principal o padre
 * @param  {string: selector} container El contenedor directo de la imagen
 * @param  {string: selector} image     La clase de la imagen que requiere hacer zoom 
 * @return {object}     The function on executes returns an object. 
 */
 
var mapZoom_gbl = mapZoom_gbl || function(parent, container, image){ 

	//variables para el uso interno de la funcion
    var incrementScale = 0.3,  // Que tanto va a aumentar con cada clic por defecto esta al 30%  0.5 = 50%
    	zoomLocations = [], // Array para guardar valores originales de posicion 
    	historyMap = [], // Guarda el historial del zoom + para cuando hagamos un decremento siga la ruta de incremento 
        countZoom = 0,  // Linea para contar zoom 5 veces 29/07/2016 
    	$parent = $(parent), // Contendor padre
    	$container = $(container),  // Contenedor Hijo 
    	$image = $(image), // Contenedor Imagen  


    	//Para acceder a las funciones del zoom 
    	publicApiZoom = { 
	    	resetZoom: resetZoomDefault,
	    	initZoom: initImageZoom,
	    	decreaseZoom: reduceZoom, 
	    	increaseZoom: moreZoom
	    };  


    function resetZoomDefault() {
    	//Establece valores por defecto de la contenedor fue creada para cuando tenemos un cambio de imagen la nueva img no inicie con valores de la anterior 
    	$container
    		.css('top', 'auto')
    		.css('left', 'auto')
    		.css('width', 'auto')
    		.css('height', 'auto');
        
        //Establece valores por defecto de imagen 
    	$image
    		.css('top', 'auto')
    		.css('left', 'auto')
    		.attr('data-zoom', 0);

    	zoomLocations = []; //limpiamos posiciones 
    	historyMap = []; //limpiamos historial 
    } 

    

    function initImageZoom() {  //Inicia el zoom de la imagen  

    	publicApiZoom.resetZoom(); // Por si quedan valores anteriores 

    	var	$parentWidth = $parent.width();

    	if($parentWidth) {

			var $imageWidth = $image[0].naturalWidth, //Obtiene el tamaño original de la imagen
    		    $imageHeight = $image[0].naturalHeight, // Alto original 

    		newHeight = Math.round($parentWidth * $imageHeight / $imageWidth);  //Calcula su alto natural con el ancho del contenedor para que sea proporcional 

    		$parent.css("height", newHeight); // Establece este alto natural para evitar mala resolucion 

    		$image
    			.css("height", newHeight)   //Se lo asigna a la imagen el alto proporcional
    			.css('width', $parentWidth); //Se asigna el ancho como es un % se convierte en px

                $("#decreaseZoom").css("display","none");  // Linea para ocultar lupa menor 29/07/2016    
    	}
    	
    } 

    /** 
     * Private function decrease Zoom
     */ 
    function reduceZoom() {

    	var $parentWidth = $parent.width(),
    		$imageWidth =  $image[0].naturalWidth, //Obtiene el tamaño original de la imagen
    		$imageHeight = $image[0].naturalHeight,


    		newHeight = Math.round($parentWidth * $imageHeight / $imageWidth), //Calcula su alto natural con el ancho del contenedor para que sea proporcional 
    		zoomCount = $image.attr('data-zoom'), // Obtiene el numero de veces que se dio click al aumento de zoom 
    		totalZoom;


    		if(zoomCount <= 0){ 

    			totalZoom = parseFloat(zoomCount); 
    		}

    		else {

                countZoom--;  //Reduce el contador para resetear imagen a las 5 veces de click  29/07/2016  
    			
    			totalZoom = parseFloat(zoomCount) - 1;
    			
    			$image.attr('data-zoom', totalZoom); // Lleva la cuenta de los clic en aumento de zoom 

    			var scaleZoom = totalZoom * incrementScale,
    			newImgWidth = $parentWidth * scaleZoom / 1 + $parentWidth,
    			newImgHeight = newHeight * scaleZoom / 1 + newHeight;

    			$image
	    			.css('width', newImgWidth)
	    			.css('height', newImgHeight);

    			activateZoom(newImgWidth, newImgHeight, false);

	    		if(historyMap.length == 4) { //Si es 4 para evitar que se descuadre la imagen busca sus valores iniciales de zoom

	    			$image
	    				.css('left', historyMap[0])
	    				.css('top', historyMap[1]);
	    		}

	    		if(historyMap.length > 4) {
	    			var indexMap = zoomCount * 2 - 4 //Calcula posicion de arreglo para obtener la guia del valor para reducir el zoom 
	    				indexMap1 = zoomCount * 2 - 3; 
	    			$image
	    				.css('left', historyMap[indexMap])  //Encuentra los valores para regresar el zoom por donde se aumento 
	    				.css('top', historyMap[indexMap1]); 
	    		}

    			if(totalZoom == 0) {
	    			$image
	    				.css('left', '0px')
	    				.css('top', '0px');
	    			publicApiZoom.resetZoom();  // Establece valores por defecto 
	    		}
	    	}
    }

    /**
     * Aumenta el zoom
     */
    function moreZoom() {
    	
    	var $parentWidth = $parent.width(),
    		$imageWidth = $image[0].naturalWidth,
    		$imageHeight = $image[0].naturalHeight,

    		newHeight = Math.round($parentWidth * $imageHeight / $imageWidth),  //Comparte propiedad de asignar siempre un alto proporcional 

    		zoomCount = $image.attr('data-zoom'), //Lleva la cuenta de los click del zoom aumenta para calcular proximo aumento 
    		totalZoom = parseFloat(zoomCount) + 1,

    		newImgWidth = $parentWidth * ( totalZoom * incrementScale) / 1 + $parentWidth, 
    		newImgHeight = newHeight * ( totalZoom * incrementScale) / 1 + newHeight;

    		$image.attr("data-zoom", totalZoom);
    		
    		$image
    			.css('width', newImgWidth)
    			.css('height', newImgHeight);

    		activateZoom(newImgWidth, newImgHeight, true); 
    }

    /**
    * Esta funcion recibe 3 paramentros ancho alto y un valor verdadero vara verificar si se quiere aumentar el zoom o solo activarlo
    */


    function activateZoom(width, height, bool) {  

    	var $parentWidth = $parent.width(),
    		$parentHeight = $parent.height(),

    		$imageWidth = $image[0].naturalWidth,
    		$imageHeight = $image[0].naturalHeight,

    		estimateHeight = height - $parentHeight, // Calcula cuantos pixeles sobran entre el contenedor padre y la imagen con aumento 
    		estimateWidth = width - $parentWidth; // Calcula cuantos pixeles sobran entre el contenedor padre y la imagen con aumento 

    		zoomLocations.push(estimateWidth);
    		zoomLocations.push(estimateHeight);

    		position = $image.position(),
    		zoomCount = $image.attr('data-zoom'); 

    		if(bool) { //Si se quiere aumentar zoom 

                countZoom++;    //Aumenta cuando el zoom aumenta  29/07/2016  

    			var calcWidth = zoomCount * zoomLocations[0],
    				calcHeight = zoomCount * zoomLocations[1],

    				heightAplied = calcWidth - zoomLocations[1], 
    				heightLastCalc = calcWidth * ((heightAplied - position.top  * 100) / heightAplied) / 100, 

    				widthAplied = calcHeight - zoomLocations[1], 
    				widthLastCalc = calcHeight * ((widthAplied - position.left  * 100) / widthAplied) / 100; //Cuando se mueve la imagen y se realiza el zoom calcula en donde se tiene que hacer el zoom

    				if(widthLastCalc < 0) { 
    					widthLastCalc *= -1; // Pasa el valor a negativo para evitar areas en blanco en zoom
    				}

    				if(heightLastCalc < 0) {
    					heightLastCalc *= -1; 
    				}

    				if(historyMap.length >= 2) { //Si el zoom solo se activo pocas veces obtiene el centro del zoom
    					$image
    						.css('top', heightLastCalc)
    						.css('left', widthLastCalc);

    					historyMap.push(widthLastCalc);
    					historyMap.push(heightLastCalc);

    				} else {

    					historyMap.push(estimateWidth / 2);
    					historyMap.push(estimateHeight / 2);

    					$image
    						.css('top', estimateHeight/2)
    						.css('left', estimateWidth/2);

    				}

        }

    		$container
    			.css('top', -1 * (height - $parentHeight)) //Asigna valores para el contenedor secundario  
    			.css('left', -1 * (width - $parentWidth)) 
    			.css('width', width * 2 - $parentWidth )
    			.css('height', height *2 - $parentHeight); 

    		$image.draggable({
    			containment: $container  //para poder arrastrar y soltar imagen jquery ui 
    		});

            $("#decreaseZoom").css("display","block");  //Muestra lupa de reducir  29/07/2016 
            if (countZoom % 5 == 0)  { imapZoom.initZoom(); }    //Si es multiplo reinicia el zoom 29/07/2016  

    }

    return publicApiZoom;  // Para acceder a las funciones 
}


/* Establece el comportamiento del zoom como sus ajustes de contenedores, aumento y decremento */ 

var imapZoom = mapZoom_gbl('#primaryZoomContainer', '#secondaryZoomContainer', '#targetZoom'); 

    imapZoom.initZoom();  //Inicia el zoom 

    $('#increaseZoom').click(function(){
        imapZoom.increaseZoom();  // Aumenta el zoom
    });
    
    $('#decreaseZoom').click(function(){
        imapZoom.decreaseZoom();  // Reduce el zoom 
    });
    
    window.addEventListener("resize", function(){
            
    imapZoom.initZoom();   // Si la ventana cambia de resolucion se cambian los valores para evitar un mal funcionamiento 
    
    }); 
