
// Funcion cambiar la imagen en product-detail.*  

  function changeImageDiv(imageChange) {   

    sourceImage = $(imageChange).attr('data-img');  //obtiene la url para cambiar despues el atributo src

    $('.mapzoom.target').attr("src", ""+sourceImage+"");    // cambia el atributo src para cambiar la imagen 
   
    $(".imgThumbs ul li").css("border-bottom-width", "1px").css("margin-bottom", "20px"); //Evita el salto de 4px cuando se cambia de imagen
 
    $(imageChange).parent().css("border-bottom-width", "5px").css("margin-bottom", "16px");  // Sombrea la parte inferior de la imagen            
   
    imapZoom.resetZoom();  imapZoom.initZoom();   // Inicia el zoom con la nueva imagen objetivo 

                                       } 

 // Funcion para iniciar zoom  



// Select simulado en listas para mostrar valores numericos o textos en la mascara de usuario al final agrega un valor numerico a un input para que este pueda ser obtenido y enviado 
// Todas las funciones son iguales solo cambian sus atributos 

function size_change(dataSize) { 

     clave = $(dataSize).attr('data-size'); 
     hide_list = $(dataSize).attr('data-list');
   
    /*muestra y oculta*/
    $(".sEmpty."+hide_list+"").removeClass("hide"); 
    $(".sEmpty."+hide_list+"").addClass("show"); 
    $(".pretty_list."+hide_list+"").removeClass("show");  
    $(".pretty_list."+hide_list+"").addClass("hide");  
    /*muestra y oculta*/ 

    $(".visiValue."+hide_list+"").text(clave); //Obtiene el texto para la mascara y colocar un elemento visual como un mes o un año 

    $("#getSize").val(clave); // Lo asigna a un input para poder ser manipulado 

                               } 



function qty_change(dataQty) { 

     clave = $(dataQty).attr('data-qty');
     hide_list = $(dataQty).attr('data-list');
   
    $(".sEmpty."+hide_list+"").removeClass("hide"); 
    $(".sEmpty."+hide_list+"").addClass("show"); 
 

    $(".pretty_list."+hide_list+"").removeClass("show"); 
    $(".pretty_list."+hide_list+"").addClass("hide");  

    $(".visiValue."+hide_list+"").text(clave); 
 
    $("#getQty").val(clave);  

                             } 




function date_change(dataDates) {

     clave = $(dataDates).attr('data-nm');
     hide_list = $(dataDates).attr('data-list');
     tar_list  = $(dataDates).attr('data-tar');
     valueDate = $(dataDates).text();  
   
    $(".sEmpty."+hide_list+"").removeClass("hide"); 
    $(".sEmpty."+hide_list+"").addClass("show"); 
    $(".pretty_list."+hide_list+"").removeClass("show"); 
    $(".pretty_list."+hide_list+"").addClass("hide");  

    $(".visiValue."+hide_list+"").text(valueDate);  
 
    $("#"+tar_list+"").val(clave);   

                               } 
 

// Termina select simulado 




  
// Funcion para collapse de opciones de producto remplaza al original de Bootstrap compatible Jquery 1.7.2

  $(document).ready(function() { 

    $("#detalles h3").removeClass(); //Limpia la clase 

    $("#detalles h3").click(function() {  // Si le dan clic activa la funcion 

        $("#detalles .comun_collapse").css("display", "none"); // Esconde todos los elementos

         abierto = $(this).attr('class'); // Obtiene clase para conocer el status de ese collapse 

        $("#detalles h3").removeClass(); // Limpia la clase para evitar confusion en el if 

        if (abierto != 'undefined') {  // Si no se encuentra le asigna un valor abierto 
            $(this).toggleClass('abierto');
            $(this).next(".comun_collapse").css("display", "block");
        }


        if (abierto == 'abierto') {  // Cuando le damos clic al mismo elemento oculta y despliega - oculta y despliega  
            $(this).toggleClass('abierto');
            $(this).next(".comun_collapse").css("display", "none");
        }

                                     });


    $("#first_thumb").trigger("click");  // inicia imagen zoom detalle producto
    $("#first").trigger("click"); //Inicia el collapse 


                                }); 




