<?php include('./header.php');  ?> 

<div class="commonTop">
   <div class="container-fluid marginQuery">
      <div class="row">
         <div class="col-xs-12">
            <h1>THE CITY BOOTIE</h1>
            <p class="catalog">
               Nothing's cooler than throwing on a pair of rugged booties. This spring, choose from peep-toe, low ankle and lace-up styles to toughen up your bare looks.
            </p>
         </div>
      </div>
   </div>
</div>
<div class="topSlimBlack">
   <div class="container-fluid marginQuery">
      <div class="row">
         <div class="col-xs-12 ">
            <ul class="sub_detail">
               <li> <a class="blueSelected" href="#"> ALL BOOTIES </a> </li>
               <li> <a href="#"> DRESS </a> </li>
               <li> <a href="#"> LACE-UP </a> </li>
               <li> <a href="#"> OPEN-TOE </a> </li>
               <li> <a href="#"> BOOTIES </a> </li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid marginQuery">
   <!-- cierra en footer.php --> 
   <div class="row">
      <div class="col-xs-12">
         <div class="more_flt">
            <p>-</p>
         </div>
         <div class="first_select">
            <div class="filter_by">
               <p>SORT BY: </p> 
            </div> 

            <select name="#" id="more_pad">
               <option value="value">Default</option>
               <option value="value">VALUE</option>
               <option value="value">VALUE</option>
               <option value="value">VALUE</option>
            </select>
         
         </div>
         <!-- first select --> 
         <div class="filter_mdl"></div>
      </div>
   </div> 
   <div class="row">
      <div class="col-xs-12 pd_filter">
         <ul class="breadcrumb">
            <li> <a href="#"> WOMEN'S</a> </li>
            <li class="delim"><span>/</span></li>
            <li class="final"> <a href="#"> BOOTIE </a></li>
         </ul>  
         <div class="pagination top">
            <span><span class="showCount">BOOTIES - 24</span> Items</span>
            <a href="#" class="view_pagination">View All</a>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#"  
                  src="./images/catalog/s_1.jpg" 
                  onmouseover="this.src='./images/catalog/s_2.jpg'" 
                  onmouseout="this.src='./images/catalog/s_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">TROOPA2-0</p>
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div> 
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge sale"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <a href="./product-detail.html"><img class="img-responsive" alt="#"
                  src="./images/catalog/b_1.jpg" 
                  onmouseover="this.src='./images/catalog/b_2.jpg'" 
                  onmouseout="this.src='./images/catalog/b_1.jpg';"></a>
            </div>
            <div class="product_inf">
               <p class="product_name">CHESCKA</p> 
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">    
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#" 
                  src="./images/catalog/a_1.jpg" 
                  onmouseover="this.src='./images/catalog/a_2.jpg'" 
                  onmouseout="this.src='./images/catalog/a_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">AUSTIN</p>
               <p class="product_price">$ 109.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#" 
                  src="./images/catalog/c_1.jpg" 
                  onmouseover="this.src='./images/catalog/c_2.jpg'" 
                  onmouseout="this.src='./images/catalog/c_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">KENNEE</p>
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
   </div>
   <!-- row -->
   <div class="row">
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#" 
                  src="./images/catalog/s_1.jpg" 
                  onmouseover="this.src='./images/catalog/s_2.jpg'" 
                  onmouseout="this.src='./images/catalog/s_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">TROOPA2-0</p>
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
         <!-- prd all --> 
      </div>
      <div class="col-xs-3 ">
         <div class="resumeProduct"> 
            <div class="js_badge sale"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <a href="./product-detail.html"><img class="img-responsive" alt="#" 
                  src="./images/catalog/b_1.jpg" 
                  onmouseover="this.src='./images/catalog/b_2.jpg'" 
                  onmouseout="this.src='./images/catalog/b_1.jpg';"></a>
            </div>
            <div class="product_inf"> 
               <p class="product_name">CHESCKA</p>
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#" 
                  src="./images/catalog/a_1.jpg" 
                  onmouseover="this.src='./images/catalog/a_2.jpg'" 
                  onmouseout="this.src='./images/catalog/a_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">AUSTIN</p>
               <p class="product_price">$ 109.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
      <div class="col-xs-3 ">
         <div class="resumeProduct">
            <div class="js_badge new"></div>
            <div class="js_loves"> <a href="#"> <span class="heart"></span> </a> </div>
            <div class="prd_image">  
               <img class="img-responsive" alt="#" 
                  src="./images/catalog/c_1.jpg" 
                  onmouseover="this.src='./images/catalog/c_2.jpg'" 
                  onmouseout="this.src='./images/catalog/c_1.jpg';">
            </div>
            <div class="product_inf">
               <p class="product_name">KENNEE</p>
               <p class="product_price">$ 933.00</p>
               <div class="more_texture">   
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
                  <img src="./images/default/temporal_descarga.jpg" alt="#">
               </div>
            </div>
            <div class="pr_review">
               <input class="common-button q_view"  type="submit" value="+ QUICK VIEW" >
            </div>
         </div>
      </div>
   </div>
   <!-- row -->
   <div class="btm_catalog">
      <div class="showingCount"><span>Showing</span> <span class="fontLight18"> 1 - 20</span> </div>
      <div class="btn_catalog"><a href="#" class="view_pagination">View More</a> <a href="#" class="view_pagination">View All</a> </div>
   </div>
</div>
<!-- container-fluid marginQuery --> 
<?php include('./footer.php');  ?>