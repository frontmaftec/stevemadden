<?php include('./header.php');  ?> 
<div class="noTermFound">
   <div class="container-fluid marginQuery">
      <div class="col-xs-7 no-terms">
         <h2>SEARCH RESULTS</h2>
         <h2>WE'RE SORRY. WE WERE UNABLE TO FIND ANY PRODUCTS </h2>
         <h2>THAT MATCHED <span class="blue"> TERM OF SEARCH </span></h2>
         <div class="box-term"> 
            <input class="user-search" name="" type="text" value="" size="20" maxlength="65"> 
            <input class="common-button white" id="" type="submit" value="GO">   
         </div>
      </div>
      <div class="col-xs-5">
         <h2 class="need-white">NEED SOME HELP? CHAT WITH ONE OF OUR ASSOCIATES.</h2>
      </div>
   </div>
</div>
<div class="container-fluid marginQuery">
   <?php include('./multi_slide.php'); ?>
</div>
<?php include('./footer.php');  ?>