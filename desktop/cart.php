<?php include('./header.php');  ?>       
 
<div class="topMarginCart">   </div>

<div class="container-fluid marginQuery">
   <div class="row">
      <div class="col-xs-12">
         <a class="linkBackShop" href="#">
            <p>CONTINUE SHOPPING</p>
         </a> 
      </div>
   </div>
   <div class="row">
      <div class="col-xs-8">
         <!-- columnas --> 
         <h1 class="title_shopping">SHOPPING BAG</h1>
         <div class="row">
            <div class="col-xs-6">
               <div class="textDescriptionCart">
                  <p class="regular">Items will be saved in your Shopping Bag for 60 days, however, we cannot guarantee the inventory availability at that time. 
                     <span style="font-weight:bold">Items in your shopping cart are not reserved until you place your order.</span> 
                     If you are signed in, your items may be viewed on your computer, tablet, or mobile phone.&nbsp;Need Help?&nbsp;&nbsp;Call Customer Service at 1-888-SMADDEN 7 days a week, 9am to 10pm ET or 
                     <a href="#">contact us</a> via e-mail any time.
                  </p>
                  <div class="payOptions">
                     <!--
                        <div class="col_paypal">  
                           <img src="./images/content/paypal.png" alt="">
                           <span class="fontLight18"> OR </span>  
                        </div> -->   
                     <div class="col_begin"> 
                        <input class="common-button" id="emailSignUpSubmit" type="submit" value="Begin Checkout" autofillparam="ON">
                     </div>
                  </div>
                  <!-- pay options -->
               </div>
            </div>
            <div class="col-xs-6">
               <div class="row">
                  <div class="col-xs-2"> 
                     <a class="c_iconshop" href="#"><img src="./images/content/bag-id.png" alt=""></a>
                  </div>
                  <div class="col-xs-10">
                     <p class="regular">If calling about your shopping bag or an order that you are trying to place, <a href="#">click here</a> to obtain your Shopping Bag ID. The Customer Service Representative that assists you may ask for it.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--x-8--> 
      <div class="col-xs-4 cart_left">
         <img src="./images/content/shipping.gif" alt="">
      </div>
   </div>
   <!-- row --> 
   <div class="resume_cart">
      <div class="row cart_detail">
         <div class="col-xs-2"> ITEM </div>
         <div class="col-xs-3"> ITEM DESCRIPTION </div>
         <div class="col-xs-3"> AVAILABILITY </div>
         <div class="col-xs-1 resetPadding"> ITEM PRICE </div>
         <div class="col-xs-1 resetPadding"> CANTIDAD </div>
         <div class="col-xs-2 text-right"> TOTAL PRICE </div>
      </div>
      <!-- Producto Agregado -->
      <div class="row">
         <div class="col-xs-2 paddingAuto"> 
            <img src="./images/content/bag_shop.jpg" alt=""> 
         </div>
         <div class="col-xs-3 data_product paddingAuto">
            <h3>BSHEENAA</h3>
            <h4>COLOR: <span>GREY</span></h4>
            <h4>SIZE: <span>ONESZ</span></h4>
         </div>
         <div class="col-xs-3 paddingAuto">
            <p class="fontLight18 description">Pre-Order, Expected Ship Date: 7/24/16. Please Note Your Card Will Not Be Charged Until The Item Has Shipped.</p>
         </div>
         <div class="col-xs-1 paddingAuto removePdLeft"> 
            <span class="fontLight18 price"> $98.00 </span>  
         </div>
         <div class="col-xs-1 paddingAuto resetPadding linkQty">
            <p>1</p>
            <a href="#"> EDTIT ITEM </a>
            <a href="#"> REMOVE ITEM </a>  
         </div>
         <div class="col-xs-2 paddingAuto">
            <p class="text-right">$98.00</p>
         </div>
      </div>
      <!-- Producto Agregado -->
      <div class="bottom_margin_cart"> </div>
   </div>
   <!-- resumen cart -->  
   <div class="row">
      <div class="col-xs-6">
            <input class="user-promocode" name="promoCode" type="text" placeholder="Promo Code">
            <input class="common-button"  type="submit" value="APPLY" autofillparam="ON">
            <p class="one_promo">*ONE PROMO CODE MAY BE USED PER ORDER</p>
  
      </div>

      <div class="col-xs-6">
         <div class="row">
            <div class="col-xs-12">
                  <table class="table f_cart">
                     <tr>
                        <td>
                           <p class="fontLight18 m_cart up">MERCHANDISE:</p>
                        </td>
                        <td>
                           <p class="fontLight18 m_cart text-right">$189.95</p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p class="fontLight18 m_cart up sub">Estimated Shipping:</p>
                        </td>
                        <td>
                           <p class="fontLight18 m_cart text-right">
                              <select class="form-control" id="sel1" style=" padding: 8px;">
                                 <option>Standard shipping $7.95</option>
                                 <option>2</option>
                                 <option>3</option>
                                 <option>4</option>
                              </select>
                           </p>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p class="fontLight18 m_cart up">Estimated Sales Tax:</p>
                        </td>
                        <td>
                           <p class="fontLight18 m_cart text-right">$0.00</p>
                        </td>
                     </tr>
                  </table>
                  <div class="neutral_border"></div>
                  <table class="table f_cart">
                     <!-- tabla datos pago -->
                     <tr>
                        <td>
                           <p class="fontLight18 m_cart"> 
                           <h2> Estimated Order Total:</h2>
                           </p> 
                        </td>
                        <td>
                           <h2 class="text-right">$195.22</h2>
                           </p> 
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <p class="fontLight18"> Final shipping and tax will be calculated during checkout based on shipping addresses </p>
                        </td>
                        <td>
                           <p class="fontLight18 m_cart text-right">
                           <div class="text-right">
                              <!--
                                 <div class="col_paypal">  
                                    <img src="./images/content/paypal.png" alt="">
                                    <span class="fontLight18"> OR </span>  
                                 </div>--> 
                              <div class="col_begin"> 
                                 <input class="common-button" id="emailSignUpSubmit" type="submit" value="Begin Checkout" autofillparam="ON">
                              </div>
                           </div>
                           </p>
                        </td>
                     </tr>
                  </table>
                  <!-- tabla datos pago --> 
                  <p class="red_notice">Expedited Shipping orders must be placed by 1pm ET to ship the same day.</p>
               </div>
            </div>
      </div>
      <!-- col 6 -->
   </div>
   <!-- row --> 
   <!-- LIKE --> 
   <div class="row">
      <div class="col-xs-6 resetPadding">
         <h1 class="you_may" >YOU MAY ALSO LIKE</h1>
      </div>
      <div class="col-xs-6 resetPadding">
         <div class="more_controlers">
            <div class="cnt rg swiper-button-next"></div>
            <div class="cnt lf swiper-button-prev"></div>
         </div>
      </div>
   </div>
   <!-- LIKE -->  
   <div class="row">
      <div class="col-xs-12 resetPadding">
         <!-- Swiper --> 
         <div class="swiper-container swall">
            <div class="swiper-wrapper">
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/a_1.jpg" 
                     onmouseover="this.src='./images/catalog/a_2.jpg'" 
                     onmouseout="this.src='./images/catalog/a_1.jpg';">
                  <div class="titlePrice">
                     <h3> CHESCKA </h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/c_1.jpg" 
                     onmouseover="this.src='./images/catalog/c_2.jpg'" 
                     onmouseout="this.src='./images/catalog/c_1.jpg';">
                  <div class="titlePrice">
                     <h3> KENNEE</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/d_1.jpg" 
                     onmouseover="this.src='./images/catalog/d_2.jpg'" 
                     onmouseout="this.src='./images/catalog/d_1.jpg';">
                  <div class="titlePrice">
                     <h3> LOLA</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/s_1.jpg" 
                     onmouseover="this.src='./images/catalog/s_2.jpg'" 
                     onmouseout="this.src='./images/catalog/s_1.jpg';">
                  <div class="titlePrice">
                     <h3> TROOPA2-0</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/s_1.jpg" 
                     onmouseover="this.src='./images/catalog/s_2.jpg'" 
                     onmouseout="this.src='./images/catalog/s_1.jpg';">
                  <div class="titlePrice">
                     <h3> TROOPA2-0</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/s_1.jpg" 
                     onmouseover="this.src='./images/catalog/s_2.jpg'" 
                     onmouseout="this.src='./images/catalog/s_1.jpg';">
                  <div class="titlePrice">
                     <h3> TROOPA2-0</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/s_1.jpg" 
                     onmouseover="this.src='./images/catalog/s_2.jpg'" 
                     onmouseout="this.src='./images/catalog/s_1.jpg';">
                  <div class="titlePrice">
                     <h3> TROOPA2-0</h3>
                     <p>$33.22</p>
                  </div>
               </div>
               <div class="swiper-slide">
                  <div class="borderTopSwiper "></div>
                  <img class="img-responsive" 
                     src="./images/catalog/s_1.jpg" 
                     onmouseover="this.src='./images/catalog/s_2.jpg'" 
                     onmouseout="this.src='./images/catalog/s_1.jpg';">
                  <div class="titlePrice">
                     <h3> TROOPA2-0</h3>
                     <p>$33.22</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- col-xs-12 -->
   </div>
   <!-- row --> 
   <!-- LIKE --> 
</div>
<!-- container-fluid marginQuery --> 
<?php include('./footer.php');  ?>