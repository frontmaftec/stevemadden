<?php include('./header.php');  ?>      
<div class="commonTop">
   <div class="container-fluid marginQuery">
      <div class="row">
         <div class="col-xs-12">
            <h1 class="titleAccount">MI CUENTA</h1>
            <!--<p class="catalog">
               Nothing's cooler than throwing on a pair of rugged booties. This spring, choose from peep-toe, low ankle and lace-up styles to toughen up your bare looks.
               </p>--> 
         </div>
      </div>
   </div>
</div>

<div class="container-fluid marginQuery">
   <!-- cierra en footer.php --> 
   <div class="clear_content">
      <div class="row">
         <div class="col-xs-2">
            <ul class="lineDotted">
               <li class="selected"> <a href="#"> Mis pedidos </a></li>
               <li> <a href="#"> Mis devoluciones </a></li>
               <li> <a href="#"> Mis cupones </a></li>
               <li> <a href="#"> Información de cuenta </a></li>
               <li> <a href="#"> Libreta de direcciones </a></li>
               <li> <a href="#"> Mis Tarjetas </a></li>
               <li> <a href="#">Favoritos</a></li>
               <li> <a href="#">Notificaciones via Email?</a></li>
            </ul>
         </div>
         <div class="col-xs-10 removePdRight">
            <div class="right_data">
               <div class="header_data">
                  <h3>HOLA, ERNESTO!</h3>
                  <p class="fontLight18">RESUMEN DE CUENTA</p>
               </div>
               <div class="mainUser">
                  <div class="row">
                     <div class="col-xs-1">  <img src="./images/default/ur_profile.png" alt=""> </div>
                     <div class="col-xs-11">
                        <h2 class="sub_1">INFORMACIÓN DE LA CUENTA </h2>
                        <h3 class="sub_2">_</h3>
                        <ul class="listpanelInfo">
                           <li>
                              <p>NAME: <span> Ernesto Cardenas  </span> </p>
                           </li>
                           <li>
                              <p>EMAIL: <span> ernesto@pictures.com  </span> </p>
                           </li>
                           <li>
                              <p>PASSWORD: <span> ******  </span> </p>
                           </li>
                        </ul>
                        <hr>
                        <a class="edit_link" href="#"> EDIT MY PROFILE </a>
                     </div>
                  </div>
               </div>
               <div class="mainUser">
                  <div class="row">
                     <div class="col-xs-1">  <img src="./images/default/ur_orders.png" alt=""> </div>
                     <div class="col-xs-11">
                        <h2 class="sub_1">MIS PEDIDOS </h2>
                        <h3 class="sub_2">Último pedido </h3>
                        <ul class="listpanelInfo">
                           <li>SKU:       <span> #9S0329  </span> </li>
                           <li>TOTAL:     <span> 190.00 MXN  </span> </li>
                        </ul>
                        <hr>
                        <a class="edit_link" href="#"> VER TODOS LOS PEDIDOS </a>
                     </div>
                  </div>
               </div>
               <div class="mainUser">
                  <div class="row">
                     <div class="col-xs-1">  <img src="./images/default/ur_book.png" alt=""> </div>
                     <div class="col-xs-11">
                        <h2 class="sub_1">MIS DIRECCIONES </h2>
                        <h3 class="sub_2">_</h3>
                        <ul class="listpanelInfo">
                           <li>NAME:      <span> Ernesto Cárdenas  </span> </li>
                           <li>CALLE:     <span> DEL ARTE   </span> </li>
                           <li>CIUDAD:    <span> CDMX  </span> </li>
                        </ul>
                        <hr>
                        <a class="edit_link" href="#"> VER DIRECCIONES </a>
                     </div>
                  </div>
               </div>
               <p style="margin-bottom: 80px; ">--- / TERMINA RESUMEN DE CUENTA REMOVER ESTA LINEA/ ---</p>
               <div class="user_panel">
                  <h2>MIS PEDIDOS</h2>
                  <p class="t_standar"> Usted no ha realizado Pedidos </p>
                  <div class="resume_cart">
                     <div class="row cart_detail">
                        <div class="col-xs-3"> FECHA </div>
                        <div class="col-xs-3"> PEDIDO # </div>
                        <div class="col-xs-3"> DIRECCIÓN </div>
                        <div class="col-xs-3 text-right"> TOTAL PRICE </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="row">
                        <div class="col-xs-3 data_general paddingAuto">
                           <h3>12/03/2016</h3>
                        </div>
                        <div class="col-xs-3 data_general paddingAuto">
                           <h3>399S03S9VV</h3>
                        </div>
                        <div class="col-xs-3 paddingAuto">
                           <p class="fontLight18 description">Calle Altitud #222</p>
                           <p class="fontLight18 description">Colonia Polanco</p>
                        </div>
                        <div class="col-xs-3 data_general paddingAuto">
                           <h3 class="text-right">$98.00</h3>
                        </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="bottom_margin_cart"> </div>
                  </div>
               </div>
               <!-- loves --> 
               <div class="user_panel">
                  <h2>MIS DEVOLUCIONES</h2>
                  <p class="t_standar"> Usted no ha realizado Devoluciones </p>
                  <div class="resume_cart">
                     <div class="row cart_detail">
                        <div class="col-xs-2"> PEDIDO # </div>
                        <div class="col-xs-2"> SKU </div>
                        <div class="col-xs-3"> FECHA DE DEVOLUCIÓN </div>
                        <div class="col-xs-3"> MOTIVO </div>
                        <div class="col-xs-2 text-right"> CODIGO RASTREO </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="row">
                        <div class="col-xs-2 data_general paddingAuto">
                           <h3> 740 </h3>
                        </div>
                        <div class="col-xs-2 data_general paddingAuto">
                           <h3>XYZN3S9202</h3>
                        </div>
                        <div class="col-xs-3 data_general paddingAuto">
                           <h3>15/10/2016</h3>
                        </div>
                        <div class="col-xs-3 paddingAuto">
                           <p class="fontLight18 description"> La talla no era la adecuada</p>
                        </div>
                        <div class="col-xs-2 data_general paddingAuto">
                           <h3 class="text-right">732839282812392</h3>
                        </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="bottom_margin_cart"> </div>
                  </div>
               </div>
               <!-- user --> 
               <div class="user_panel">
                  <h2>MIS CUPONES</h2>
                  <p class="t_standar"> Usted no tiene cupones </p>
                  <div class="resume_cart">
                     <div class="row cart_detail">
                        <div class="col-xs-6"> CLAVE CUPON # </div>
                        <div class="col-xs-6"> FECHA DE EXPIRACIÓN </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="row">
                        <div class="col-xs-6 data_general paddingAuto">
                           <h3> JULIO02DESCUENTO </h3>
                        </div>
                        <div class="col-xs-6 data_general paddingAuto">
                           <h3>31/07/2016</h3>
                        </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="bottom_margin_cart"> </div>
                  </div>
               </div>
               <!-- user panel --> 
               <div class="user_panel">
                  <h2>EDITAR CUENTA</h2>
                  <div class="row">
                     <div class="col-xs-6">
                        <p class="fontLight18 common-error">Error.</p>
                        <p class="fontLight18 common-success">Success.</p>
                        <p class="fontLight18 common-warning">Warning.</p>
                        <p class="fontLight18 common-info">Info.</p>
                        <form class="input_values" action="./">
                           <label for="inputMail">E-MAIL: </label>
                           <input class="txtStandar" name="#" id="inputMail" type="text" autocomplete="off" spellcheck="false">
                           <label for="inputSex">SEXO: </label>
                           <input class="txtStandar" name="#" id="inputSex" type="text" autocomplete="off" spellcheck="false">
                           <label for="inputName">NOMBRE: </label>
                           <input class="txtStandar" name="#" id="inputName" type="text" autocomplete="off" spellcheck="false">
                           <label for="inputSecond">APELLIDO: </label>
                           <input class="txtStandar" name="#" id="inputSecond" type="text" autocomplete="off" spellcheck="false">
                           <label> CUMPLEAÑOS: </label> 
                           <div class="row">
                              <div class="col-xs-4 setOfDates">
                                 <div class="sEmpty f1 show" onclick="hidePrettySelect('f1')"> <span class="visiValue f1 nf">DD</span> <span class="sArrow"></span> </div>
                                 <ul class="pretty_list f1">
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="01" onclick="date_change(this)">
                                          <p>01</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="02" onclick="date_change(this)">
                                          <p>02</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="03" onclick="date_change(this)">
                                          <p>03</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="04" onclick="date_change(this)">
                                          <p>04</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="05" onclick="date_change(this)">
                                          <p>05</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="06" onclick="date_change(this)">
                                          <p>06</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="07" onclick="date_change(this)">
                                          <p>07</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="08" onclick="date_change(this)">
                                          <p>08</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="09" onclick="date_change(this)">
                                          <p>09</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="10" onclick="date_change(this)">
                                          <p>10</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="11" onclick="date_change(this)">
                                          <p>11</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getDD" data-list="f1" data-nm="12" onclick="date_change(this)">
                                          <p>12</p>
                                       </a>
                                    </li>
                                 </ul>
                                 <input id="getDD" type="hidden" value="">  
                              </div>
                              <div class="col-xs-4 setOfDates">
                                 <div class="sEmpty f2 show" onclick="hidePrettySelect('f2')"> <span class="visiValue f2 nf">MM</span> <span class="sArrow"></span> </div>
                                 <ul class="pretty_list f2">
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="01" onclick="date_change(this)">
                                          <p>ENE</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="02" onclick="date_change(this)">
                                          <p>FEB</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="03" onclick="date_change(this)">
                                          <p>MAR</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="04" onclick="date_change(this)">
                                          <p>ABR</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="05" onclick="date_change(this)">
                                          <p>MAY</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="06" onclick="date_change(this)">
                                          <p>JUN</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="07" onclick="date_change(this)">
                                          <p>JUL</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="08" onclick="date_change(this)">
                                          <p>AGO</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="09" onclick="date_change(this)">
                                          <p>SEP</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="10" onclick="date_change(this)">
                                          <p>OCT</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="11" onclick="date_change(this)">
                                          <p>NOV</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getMM" data-list="f2" data-nm="12" onclick="date_change(this)">
                                          <p>DIC</p>
                                       </a>
                                    </li>
                                 </ul>
                                 <input id="getMM" type="hidden" value="">  
                              </div>
                              <div class="col-xs-4 setOfDates">
                                 <div class="sEmpty f3 show" onclick="hidePrettySelect('f3')"> <span class="visiValue f3 nf">AAAA</span> <span class="sArrow"></span> </div>
                                 <ul class="pretty_list f3">
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1990" onclick="date_change(this)">
                                          <p>1990</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1991" onclick="date_change(this)">
                                          <p>1991</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1992" onclick="date_change(this)">
                                          <p>1992</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1993" onclick="date_change(this)">
                                          <p>1993</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1994" onclick="date_change(this)">
                                          <p>1994</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1995" onclick="date_change(this)">
                                          <p>1995</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1996" onclick="date_change(this)">
                                          <p>1996</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1997" onclick="date_change(this)">
                                          <p>1997</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1998" onclick="date_change(this)">
                                          <p>1998</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="1999" onclick="date_change(this)">
                                          <p>1999</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="2000" onclick="date_change(this)">
                                          <p>2000</p>
                                       </a>
                                    </li>
                                    <li class="nf">
                                       <a href="#/" data-tar="getAA" data-list="f3" data-nm="2001" onclick="date_change(this)">
                                          <p>2001</p>
                                       </a>
                                    </li>
                                 </ul>
                                 <input id="getAA" type="hidden" value="">  
                              </div>
                           </div>
                        </form>
                        <input class="common-button pdn" id="noRepeat1" type="submit" value="GUARDAR"> 
                     </div>
                     <div class="col-xs-6">   </div>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h2>INFORMACIÓN DE LA CUENTA</h2>
                  <div class="row">
                     <div class="col-xs-12">
                        <ul class="listpanelInfo">
                           <li>
                              <p>SEXO: <span> Masculino  </span> </p>
                           </li>
                           <li>
                              <p>E-MAIL: <span> ernesto@pictures.com  </span> </p>
                           </li>
                           <li>
                              <p>NOMBRE: <span> Ernesto Cárdenas  </span> </p>
                           </li>
                           <li>
                              <p>FECHA DE NACIMIENTO: <span> 15/10/1990  </span> </p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h2>ADMINISTRAR NEWSLETTER</h2>
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="select_news">
                           <input style="margin-right: 5px;" id="nb1" value="nb1" name="nb1" type="checkbox">
                           <label for="nb1" class="#">Newsletter</label>
                           <br> 
                           <input style="margin-right: 5px;" id="nb2" value="nb2" name="nb2" type="checkbox">
                           <label for="nb2" class="#">Notificaciones De  Marketing</label>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h2>LIBRETA DE DIRECCIONES</h2>
                  <div class="row w_center">
                     <div class="col-xs-12 error-section">
                        <p class="fontLight18 common-error">Error.</p>
                        <p class="fontLight18 common-success">Success.</p>
                        <p class="fontLight18 common-warning">Warning.</p>
                        <p class="fontLight18 common-info">Info.</p>
                     </div>
                     <form class="input_values" action="./">
                        <div class="col-xs-6">
                           <label for="libretaNombre">* NOMBRE: </label>
                           <input class="txtStandar w10" name="#" id="libretaNombre" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaApellido">* APELLIDO: </label>
                           <input class="txtStandar w10" name="#" id="libretaApellido" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaTelefono">* TÉLEFONO: </label>
                           <input class="txtStandar w10" name="#" id="libretaTelefono" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaAdicional">* TÉLEFONO ADICIONAL: </label> 
                           <input class="txtStandar w10" name="#" id="libretaAdicional" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaPostal">* CÓDIGO POSTAL: </label> 
                           <input class="txtStandar w10" name="#" id="libretaPostal" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaCalle">* CALLE: </label> 
                           <input class="txtStandar w10" name="#" id="libretaCalle" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaExterior">* NO EXTERIOR: </label> 
                           <input class="txtStandar w10" name="#" id="libretaExterior" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-6">  
                           <label for="libretaInterior">* NO INTERIOR: </label> 
                           <input class="txtStandar w10" name="#" id="libretaInterior" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-12">  
                           <label for="libretaCalles">* ENTRE CALLES: </label> 
                           <input class="txtStandar w10" name="#" id="libretaCalles" type="text" autocomplete="off" spellcheck="false">
                        </div>
                        <div class="col-xs-12 setOfDates">
                           <label>COLONIA:</label>
                           <div class="sEmpty u1 show" onclick="hidePrettySelect('u1')"> <span class="visiValue u1 nf">Vacio..</span> <span class="sArrow"></span> </div>
                           <ul class="pretty_list u1">
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="01" onclick="date_change(this)">
                                    <p>COL1</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="02" onclick="date_change(this)">
                                    <p>COL2</p> 
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="03" onclick="date_change(this)">
                                    <p>COL3</p>
                                 </a>
                              </li> 
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="04" onclick="date_change(this)">
                                    <p>COL4</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="05" onclick="date_change(this)">
                                    <p>COL5</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="06" onclick="date_change(this)">
                                    <p>COL6</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="07" onclick="date_change(this)">
                                    <p>COL7</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="08" onclick="date_change(this)">
                                    <p>COL8</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="09" onclick="date_change(this)">
                                    <p>COL9</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="10" onclick="date_change(this)">
                                    <p>CO10</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="11" onclick="date_change(this)">
                                    <p>CO11</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCOL" data-list="u1" data-nm="12" onclick="date_change(this)">
                                    <p>CO12</p>
                                 </a>
                              </li>
                           </ul>
                           <input id="getCOL" type="hidden" value="">     
                        </div>
                        <div class="col-xs-6 setOfDates">
                           <label>ESTADO: </label> 
                           <div class="sEmpty u2 show" onclick="hidePrettySelect('u2')"> <span class="visiValue u2 nf">Vacio..</span> <span class="sArrow"></span> </div>
                           <ul class="pretty_list u2">
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="01" onclick="date_change(this)">
                                    <p>AGS</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="02" onclick="date_change(this)">
                                    <p>JALI</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="03" onclick="date_change(this)">
                                    <p>CDMX</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="04" onclick="date_change(this)">
                                    <p>CHIA</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="05" onclick="date_change(this)">
                                    <p>BAJA</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="06" onclick="date_change(this)">
                                    <p>MONT</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="07" onclick="date_change(this)">
                                    <p>QUIN</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getEST" data-list="u2" data-nm="08" onclick="date_change(this)">
                                    <p>TAMP</p>
                                 </a>
                              </li>
                           </ul>
                           <input id="getEST" type="hidden" value="">    
                        </div>
                        <div class="col-xs-6 setOfDates">
                           <label>CIUDAD:</label> 
                           <div class="sEmpty u3 show" onclick="hidePrettySelect('u3')"> <span class="visiValue u3 nf">Vacio..</span> <span class="sArrow"></span> </div>
                           <ul class="pretty_list u3">
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="01" onclick="date_change(this)">
                                    <p>CDMX</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="02" onclick="date_change(this)">
                                    <p>AGS</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="03" onclick="date_change(this)">
                                    <p>MONT</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="04" onclick="date_change(this)">
                                    <p>TAMP</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="05" onclick="date_change(this)">
                                    <p>AGS</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="06" onclick="date_change(this)">
                                    <p>QUIN</p>
                                 </a>
                              </li>
                              <li class="nf">
                                 <a href="#/" data-tar="getCD" data-list="u3" data-nm="07" onclick="date_change(this)">
                                    <p>CHIA</p>
                                 </a>
                              </li>
                           </ul>
                           <input id="getCD" type="hidden" value="">   
                        </div>
                     </form>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h2>MIS CONSULTAS</h2>
                  <div class="row">
                     <div class="col-xs-4"> 
                        <label for="#">* TITULO: </label>
                        <input class="txtStandar w10" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
                        <label for="consultaMessage">* MENSAJE: </label>
                        <textarea class="txt-msg" id="consultaMessage" rows="6">    </textarea>  
                     </div>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h2>MIS TARJETAS</h2>
                  <p class="t_standar"> Usted no tiene tarjetas </p>
                  <div class="resume_cart">
                     <div class="row cart_detail">
                        <div class="col-xs-4"> TARJETA </div>
                        <div class="col-xs-4"> TERMINACIÓN </div>
                        <div class="col-xs-4"> COMPRAS </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="row">
                        <div class="col-xs-4 data_general paddingAuto">
                           <h3> VISA </h3>
                        </div>
                        <div class="col-xs-4 data_general paddingAuto">
                           <h3>... .... 232</h3>
                        </div>
                        <div class="col-xs-4 data_general paddingAuto">
                           <h3>05</h3>
                        </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="bottom_margin_cart"> </div>
                  </div>
               </div>
               <!-- user panel --> 
               <div class="user_panel">
                  <h2>FAVORITOS</h2>
                  <p class="t_standar"> Mostrando Favoritos </p>
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="marginPixel"></div>
                        <div class="row">
                           <div class="col-xs-5">
                              <img class="img-responsive" src="http://s7d9.scene7.com/is/image/SteveMadden/STEVEMADDEN-HANDBAGS_BACADMY_GOLD?$MR%2DTHUMB$" alt=""> 
                           </div>
                           <div class="col-xs-7">
                              <div class="loveInfo">
                                 <h4>Bacadmy</h4>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="nf more_info">Texto de informacion de mas</p>
                                 <a class="love_a" href="#">REMOVE</a>
                                 <div class="priceRight">
                                    <span class="fontLight18 r_price">$58.00 </span>
                                    <span class="fontLight18">Pre-Order</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- col-x6 -->
                     <div class="col-xs-6">
                        <div class="marginPixel"></div>
                        <div class="row">
                           <div class="col-xs-5">
                              <img class="img-responsive" src="http://s7d9.scene7.com/is/image/SteveMadden/STEVEMADDEN-HANDBAGS_BACADMY_GOLD?$MR%2DTHUMB$" alt=""> 
                           </div>
                           <div class="col-xs-7">
                              <div class="loveInfo">
                                 <h4>Bacadmy</h4>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="info">dato: <span class="fontLight18">dato respuesta</span></p>
                                 <p class="nf more_info">Texto de informacion de mas</p>
                                 <a class="love_a" href="#">REMOVE</a>
                                 <div class="priceRight">
                                    <span class="fontLight18 r_price">$58.00 </span>
                                    <span class="fontLight18">Pre-Order</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- col-x6 -->
                  </div>
               </div>
               <!-- user panel --> 
               <hr>





               <!-- user panel  29/07/2016 --> 
               <div class="user_panel">
                  <h3 class="pedidoTitlePanel"> Pedido #030202 de 17/02/16 10:33 </h3>
                  <hr>
                  <p class="t_standar"> Mostrando Favoritos </p>
                  <div class="row">
                     <div class="stepShop">
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>1</h1>
                              <p class="stepsFont green">Pedido Recibido</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>2</h1>
                              <p class="stepsFont green">Pago Recibido</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>3</h1>
                              <p class="stepsFont green">Pedido Enviado</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-gray col-step lastStep">
                              <h1>4</h1>
                              <p class="stepsFont gray">Entrega Pendiente</p>
                           </div>
                        </div> 
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-6">
                        <h3 class="titleStepShop"> Dirección de Facturación </h3>
                        <table class="table tabla-shop">
                           <thead></thead>
                           <tbody>
                              <tr>
                                 <td><b>Nombre</b></td>
                                 <td>Ernesto</td>
                              </tr>
                              <tr>
                                 <td> <b> Dirección </b> </td>
                                 <td>Arte 222</td>
                              </tr>
                              <tr>
                                 <td> <b> Colonia </b> </td>
                                 <td>Aragon la Villa</td>
                              </tr>
                              <tr>
                                 <td> <b> Estado </b> </td>
                                 <td>D.F</td>
                              </tr>
                              <tr>
                                 <td> <b> Código Postal </b> </td>
                                 <td>01150</td>
                              </tr>
                              <tr>
                                 <td><b> Télefono </b> </td>
                                 <td>00 00 00 00</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="col-xs-6">
                        <h3 class="titleStepShop"> Dirección de Facturación </h3>
                        <p> Paypal , Débito </p>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-6">
                        <h3 class="titleStepShop"> Dirección de Entrega </h3>
                        <table class="table tabla-shop">
                           <thead></thead>
                           <tbody>
                              <tr>
                                 <td><b>Nombre</b></td>
                                 <td>Ernesto</td>
                              </tr>
                              <tr>
                                 <td> <b> Dirección </b> </td>
                                 <td>Arte 222</td>
                              </tr>
                              <tr>
                                 <td> <b> Colonia </b> </td>
                                 <td>Aragon la Villa</td>
                              </tr>
                              <tr>
                                 <td> <b> Estado </b> </td>
                                 <td>D.F</td>
                              </tr>
                              <tr>
                                 <td> <b> Código Postal </b> </td>
                                 <td>01150</td>
                              </tr>
                              <tr>
                                 <td><b> Télefono </b> </td>
                                 <td>00 00 00 00</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="col-xs-6">
                        <h3 class="titleStepShop"> Rastrea tu envío </h3>
                        <p> Número de Guía: 3920202 </p>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <input class="common-button devolucionCambio" id="" type="submit" value="Devolución / Cambio">
                     </div>
                     <div class="col-xs-12">
                        <h3 class="titleStepShop"> Vendidos por Steve Madden </h3>
                        <p>1 Producto</p>
                     </div>
                     <div class="col-xs-7 nf">
                        <div class="col-xs-3">  
                           <img class="img-responsive" src="./images/product/m_1.jpg" alt="">
                        </div>
                        <div class="col-xs-5">
                           <h3 class="titleSell">Botas Steve Madden</h3>
                        </div>
                        <div class="col-xs-4">
                           <p>AN29032039203920</p>
                           <strong>Talla: 32</strong> 
                        </div>
                     </div>
                     <div class="col-xs-5 text-right nf">
                        <div class="row">
                           <div class="col-xs-3">
                              <p>Enviado</p>
                           </div>
                           <div class="col-xs-4">
                              <p>409 MXN  <strong> x1</strong> </p>
                           </div>
                           <div class="col-xs-5">
                              <p>409 MXN</p>
                           </div>
                        </div>
                        <div class="row spaceTopPrice">
                           <div class="col-xs-8">
                              <p>Subtotal</p>
                           </div>
                           <div class="col-xs-4">
                              <p class="bluePrice">409 MXN</p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-8">
                              <p>Envío</p>
                           </div>
                           <div class="col-xs-4">
                              <p class="bluePrice">409 MXN</p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-8">
                              <p>Cúpon</p>
                           </div>
                           <div class="col-xs-4">
                              <p>- 69 MXN</p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-8">
                              <p>Total</p>
                           </div>
                           <div class="col-xs-4">
                              <p class="bluePrice"><strong>309 MXN</strong></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- user panel --> 
               <hr>
               <div class="user_panel">
                  <h3 class="pedidoTitlePanel"> Orden de Devolución #902032 del 08/29/2015 22:22 </h3>
                  <hr>
                  <div class="row">
                     <div class="stepShop">
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>1</h1>
                              <p class="stepsFont green">Devolución Solicitada</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>2</h1>
                              <p class="stepsFont green">Recibo Almacén</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-green col-step">
                              <h1>3</h1>
                              <p class="stepsFont green">Cupón de devolución generado</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="col-green col-step lastStep">
                              <h1>4</h1>
                              <p class="stepsFont green">Reembolso/Cambio completo</p>
                              <img src="./images/default/rowSteps.png"> 
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12">
                        <h3 class="titleStepShop"> Información de la orden de devolución </h3>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-6">
                        <h4 class="devolucionSubTitle">Información de guia de rastreo de la devolución</h4>
                        <p>Guia de rastreo</p>
                        <p>Fecha de devolución</p>
                        <p>Razón de la devolución</p>
                     </div>
                     <div class="col-xs-6">
                        <h4 class="devolucionSubTitle">  Información del cupón de reembolso  </h4>
                        <p>Código cupón</p>
                        <p>Valor (Pesos):  3232.00 </p>
                        <p>Estatus del cupón: Este cupon ya ha sido usado</p>
                     </div>
                  </div>
                  <div class="resume_cart">
                     <div class="row cart_detail">
                        <div class="col-xs-3"> Nombre del producto </div>
                        <div class="col-xs-2"> SKU </div>
                        <div class="col-xs-2"> Estado </div>
                        <div class="col-xs-2 text-right"> Precio </div>
                        <div class="col-xs-1"> Cant </div>
                        <div class="col-xs-2"> Subtotal </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="row">
                        <div class="col-xs-3 data_general paddingAuto">
                           <h3>Playera Gris Manga</h3>
                        </div>
                        <div class="col-xs-2 data_general paddingAuto">
                           <h4>399S03S9VV</h4>
                        </div>
                        <div class="col-xs-2 paddingAuto">
                           <h4>Calle Altitud 232</h4>
                        </div>
                        <div class="col-xs-2 data_general paddingAuto">
                           <h4 class="text-right">$98.00</h4>
                        </div>
                        <div class="col-xs-1 data_general paddingAuto">
                           <h3 class="text-right">$98.00</h3>
                        </div>
                        <div class="col-xs-1 data_general paddingAuto">
                           <h3 class="text-right bluePrice">$222.00</h3>
                        </div>
                     </div>
                     <!-- Producto Agregado -->
                     <div class="bottom_margin_cart"> </div>






                     <div class="col-xs-12">
                        <input class="common-button" id="" type="submit" value="<< Regresar a lista de devoluciones">
                     </div>


               <!-- 29/07/2016 --> 




                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- container-fluid marginQuery --> 
<?php include('./footer.php');  ?>