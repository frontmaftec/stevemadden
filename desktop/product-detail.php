<?php include('./header.php');  ?>  
 
<div class="container-fluid marginQuery"> 
   <div class="row">
      <div class="col-xs-12">
         <ul class="breadcrumb">
            <li class="crumb"> <a href="#">HANDBAGS</a> </li> 
            <li class="delim"><span>/</span></li>
            <li class="crumb"> <a href="#">HANDBAGS</a> </li>
            <li class="delim"><span>/</span></li>
            <li class="crumb"> <a href="#">HANDBAGS</a> </li>
            <li class="delim"><span>/</span></li>
            <li class="crumb last">Clutches</li>
         </ul>
      </div>
   </div>
   <!-- row --> 
   <div class="row">
      <div class="col-xs-3 removePdRight">
         <h2 class="t_title">AUSTIN</h2>
         <span class="product_price"> $3.669 MXN </span>
         <div class="imgThumbs">
            <p class="w_review"> <a href="#"> WRITE A REVIEW  </a></p>
            <ul>
               <li> 
                  <a id="first_thumb" href="#/" data-img="./images/product/b_1.jpg" onclick="changeImageDiv(this)">
                  <img class="marginGray" alt="#" src="./images/product/m_1.jpg"></a>
               </li> 
               <li>  
                  <a href="#/" data-img="./images/product/b_2.jpg" onclick="changeImageDiv(this)"> 
                  <img class="marginGray" alt="#" src="./images/product/m_2.jpg"></a>
               </li>
               <li> 
                  <a href="#/" data-img="./images/product/b_3.jpg" onclick="changeImageDiv(this)">
                  <img class="marginGray" alt="#" src="./images/product/m_3.jpg"></a>
               </li>
               <li> 
                  <a href="#/" data-img="./images/product/b_4.jpg" onclick="changeImageDiv(this)">
                  <img class="marginGray" alt="#" src="./images/product/m_4.jpg"></a>
               </li>
               <li> 
                  <a href="#/" data-img="./images/product/b_5.jpg" onclick="changeImageDiv(this)">
                  <img class="marginGray" alt="#" src="./images/product/m_5.jpg"></a>
               </li>
            </ul>
         </div>
      </div>
      <!-- col-xs-3-->
      <div class="col-xs-6 removePdLeft">
         <div class="row">
            <div class="col-xs-1 resetPadding">
               <div class="js_badge new"></div>
            </div>
            <div class="col-xs-11">
       
                      
<div id="primaryZoomContainer"> 
    <div id="secondaryZoomContainer"> 
<img class="mapzoom target" alt="#" data-zoom="0" src="./images/product/b_1.jpg" id="targetZoom"/> 
    </div>
</div> 

    
                     <div class="buttons setZoomOptions">
                        <ul class="zoom_control"> 
                           <li class="zoom-in" id="increaseZoom"> <img src="./images/default/zoom_plus.png" alt=""> </li>
                           <li class="zoom-out minus" id="decreaseZoom"> <img src="./images/default/zoom_minus.png" alt=""> </li>
                        </ul>
                     </div>

            </div>
         </div>
         <!-- row -->  
      </div>
      <!--col-xs-6--> 
      <div class="col-xs-3">
         <div class="m_color">
            <p> COLOR: MULTI SNAKE - $3.699 MXN</p>
            <div class="mini_options"> 
               <a href="#"><img  class="active" src="./images/default/d_1.jpg" alt=""></a>
               <a href="#"><img  src="./images/default/d_2.jpg" alt=""></a>
            </div>
            <div class="filter_select">
               <div class="d_size">
                  <p> TALLA: </p> 
                  <!-- remplazar "s1" por un identificador personalizao no se puede repetir en la misma vista --> 
                  <div class="sEmpty s1" onclick="hidePrettySelect('s1')"> <span class="visiValue s1 nf"> Seleccionar </span> <span class="sArrow"></span> </div>
                  <ul class="pretty_list s1">
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="5" onclick="size_change(this)">
                           <p>5</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="6" onclick="size_change(this)"> 
                           <p>6</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="7" onclick="size_change(this)"> 
                           <p>7</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="8" onclick="size_change(this)"> 
                           <p>8</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="9" onclick="size_change(this)"> 
                           <p>9</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s1" data-size="10" onclick="size_change(this)"> 
                           <p>10</p>
                        </a>
                     </li>
                  </ul>
                  <input id="getSize" type="hidden" value="">
               </div>
               <div class="d_qty">
                  <p> CANTIDAD: </p>
                  <div class="sEmpty s2" onclick="hidePrettySelect('s2')"> <span class="visiValue s2 nf"> 1 </span> <span class="sArrow"></span> </div>
                  <ul class="pretty_list s2">
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="1" onclick="qty_change(this)">
                           <p>1</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="2" onclick="qty_change(this)">
                           <p>2</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="3" onclick="qty_change(this)">
                           <p>3</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="4" onclick="qty_change(this)">
                           <p>4</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="5" onclick="qty_change(this)">
                           <p>5</p>
                        </a>
                     </li>
                     <li class="nf">
                        <a href="#/" data-list="s2" data-qty="6" onclick="qty_change(this)">
                           <p>6</p>
                        </a>
                     </li>
                  </ul>
                  <input id="getQty" type="hidden" value="1"> 
               </div>
            </div>
            <div class="boton_add">  
             <input onclick="location.href='./cart.html';" class="common-button add_bag" id="#" type="submit" value="ADD TO BAG">
            </div>
            <div class="p_favoritos">
               <p><span class="heart product"></span> LOVE IT!</p>
            </div>
         </div>
      </div>
      <!--col-xs-3-->
   </div>
   <!--row--> 
   <div class="row"> 
      <div class="col-xs-6">
         <div class="marginFour"></div> 
         <div id="detalles">
            <h3 id="first" class="abierto">Descripción</h3>
            <div class="comun_collapse  default">
               <p>Chaqueta completamente bordada en canutillos dorados en todo el cuerpo y mangas.  Chaqueta con escote solapada y mangas larga. 
                  Forrada a tono. Tejido Premium. Prenda exclusiva
               </p>
            </div>
            <h3>Material</h3> 
            <div class="comun_collapse">
               <p>100% Poliéster-Polyester-Poliéster Con Aplicación/Com Aplicação Forro-Lining-Forro: 100% Viscosa-Viscose-Viscose</p>
            </div>
            <h3>Cuidado de la ropa</h3>
            <div class="comun_collapse">
               <p>Lavado En Seco Con Percloroetileno En Tintorería Tradicional - No Exprimir - No Lavar - No Planchar - No Secar En Máquina - No Usar Blanqueador -</p>
            </div>
            <h3>Medios de Pago</h3>
            <div class="comun_collapse">
               <div><img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/mercadopago.png" alt="MercadoPago"></div>
            </div>
            <h3>Métodos de Envío</h3>
            <div class="comun_collapse">
               <p>Podrás elegir entre las siguientes opciones de envío:</p>
               <ul>
                  <li>Envío estándar: vía Correos.</li>
                  <li>Retiro en sucursal.</li>
                  <li>Pick up in store – Local Rapsodia: Local DOT o Local Recoleta Mall.</li>
               </ul>
               <p>Para más información consulta <a title="Envío" href="/envio">aquí</a></p>
            </div>
            <h3>Contacto</h3>
            <div class="comun_collapse">
               <div>Atención telefónica: <a href="tel:3221-6869">3221-6869</a></div>
               <div>Escribinos a: <a href="mailto:contacto@steve.com">contacto@steve.com</a></div>
            </div>
         </div>
        
      </div>


      <div class="col-xs-6">
         <div class="p_banner">
            <img class="img-responsive" alt="#" src="./images/default/banner.jpg" alt="">  
         </div>
      </div> 
      <!-- xs-6 --> 

   </div>
   <!-- row --> 
   <?php include('./multi_slide.php'); ?>  
   
</div>
<!-- container-fluid marginQuery --> 
<?php include('./footer.php');  ?>