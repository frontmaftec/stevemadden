
<footer>
   <div class="container-fluid marginQuery mbf">
      <div class="col-xs-8">
         <div class="row" id="firstSectionFooter">
            <div class="col-xs-3">
               <h3>CUSTOMER SERVICE</h3>
               <ul class="f_op">
                  <li> <a href="#"> FAQ  </a> </li>
                  <li> <a href="#"> My Account  </a> </li>
                  <li> <a href="#"> RETURN POLICY  </a> </li>
                  <li> <a href="#"> Shipping Policy  </a> </li>
                  <li> <a href="#"> SHOE SIZE CHART   </a> </li>
                  <li> <a href="#"> CONTACT US  </a> </li>
               </ul>
            </div>
            <div class="col-xs-3">
               <h3>ABOUT THE COMPANY</h3>
               <ul class="f_op">
                  <li> <a href="#"> ABOUT STEVE MADDEN  </a>  </li>
                  <li> <a href="#"> CAREERS   </a> </li>
               </ul>
            </div>
            <div class="col-xs-3">
               <h3>SITE TERMS</h3>
               <ul class="f_op">
                  <li> <a href="#">SITE MAP </a> </li>
                  <li> <a href="#">PRIVACY POLICY</a>  </li>
               </ul>
            </div>
            <div class="col-xs-3">
               <h3>GIFT CARDS</h3>
               <ul class="f_op">
                  <li> 
                     <img src="./images/content/storelocator.png" alt="">  
                     <a href="#">STORE LOCATOR</a>  
                  </li>
               </ul>
            </div>
         </div>
         <div class="col-xs-4 chat_left"> 
            <div class="row"> 
               <a href="#"><img class="liveChatService" src="./images/default/btn-livechat.png" alt="#"> </a>
            </div>
         </div>
         <div class="col-xs-8">
            <p class="secondSectionFooter"><a href="mailto:servicioalcliente@stevemadden.com.mx">servicioalcliente@stevemadden.com.mx</a></p>
         </div>
      </div>
      <div class="col-xs-4">
         <div class="row" id="secondSectionFooter">
            <div class="col-xs-12 removePdLeft">
               <h3> E-MAIL </h3>
               <h4 class="discount">Get 10% off when you sign up for emails</h4>

               <p class="fontLight18 common-error">Error.</p> 
               <p class="fontLight18 common-success">Success.</p>

               <input class="user-email" name="UserEmail" type="email" value="" size="20" maxlength="65">
               <input class="common-button" id="emailSignUpSubmit" type="submit" value="GO"> 
               <p class="f_letter">Steven Madden, Ltd requires that our customers comply with The Children’s Online Privacy Protection Act which prohibits the collection of any information from children under the age of 13.</p>
            </div>
         </div>
         <ul class="socialIconsBlack"> 

            <li class="facebook"> 
            <a href="https://www.facebook.com/SteveMaddenMx/" title="Facebook" target="_blank">
            <span>Facebook</span>   
            </a> 
            </li>
          
            <li class="twitter">
            <a href="https://twitter.com/SteveMadden" title="Twitter" target="_blank">
            <span>Twitter</span> 
            </a> 
            </li>
           
            <li class="instagram"> 
            <a href="https://www.instagram.com/stevemaddenmx/" title="Instagram" target="_blank">
            <span>Instagram</span>
            </a> 
            </li> 


         </ul>
      </div>
   </div>
</footer> 



<script src="https://code.jquery.com/jquery-1.7.2.min.js"   integrity="sha256-R7aNzoy2gFrVs+pNJ6+SokH04ppcEqJ0yFLkNGoFALQ="   crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.7.0/jquery-ui.min.js" integrity="sha256-CkxUCXQ5bBbS+VV9O06NQWvw/odbX/kf3aTCVI86HWA=" crossorigin="anonymous"></script>
 
<script src="./js/swiper.js"></script> 
<script src="./js/start_swiper.js"></script> 

<script src="./js/product-detail.js"></script> 
<script src="./js/commonScripts.js"></script> 
 
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPPleXmRbkqgKW7c9lRCQSvTvBo7Ssmyg&signed_in=false"></script> 
<script src="./js/locationsMap.js"></script> 

<script src="./js/mapZoom.js"></script> 
<script src="./js/mapMenu.js"></script> 
 
</body> 
</html> 