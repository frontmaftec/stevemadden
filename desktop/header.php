<!DOCTYPE html> 
<html lang="es"> 
   <head>  
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Steve Madden</title>
      <!-- Bootstrap -->
      <link href="./css/general.css" rel="stylesheet"> 
      <link href="./css/swiper.css" rel="stylesheet">
      <link href="./css/mapZoom.css" rel="stylesheet">    <!-- only used in detailProduct -->   
      <link href="./images/default/favicon.ico" rel="shortcut icon">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]--> 
   </head>
   <body>
      <div class="blueTopSearch">
         <form action="./" method=""> 
            <input type="text" class="topSearchInput" placeholder="Búscar" autofocus>
            <input class="search-button fl" type="submit" value="">
         </form>
      </div>
      <header>
         <div class="container-fluid marginQuery">
            <div class="row">
               <div class="col-xs-3">
                  <a href="#"><img class="#" src="./images/content/topLeftBanner.jpg" alt="#"></a> 
               </div>
               <div class="col-xs-9">
                  <ul class="IconsSet"> 
                     <li class="loves">
                        <a href="#">
                           <p>0</p>
                        </a>
                     </li>
                     <li class="cart">
                        <a href="#">
                           <p>0</p>
                        </a>
                     </li>
                     <li class="searchBoxTopShow"></li>
                  </ul>
                  <div class="topSetLinks">  
                     <ul class="topRightOptions"> 
                        <!-- <li class="h_blue"> ABC </li> --> 
                        <li> <a href="#">Tiendas</a>   </li>
                        <li> <a href="./account.html">Mi Cuenta</a> </li> 
                     </ul>
                  </div>
               </div>
            </div> 
            <!-- row --> 
            <div class="row">
               <div class="col-xs-12">
                  <div class="centre"> <a href="#"> <img src="./images/default/stevemadden_logo.png" alt="#" class="site-logo"> </a>  </div>
               </div>
               <nav id="primeNav">  
                  <div class="centre"> 
                     <ul class="navigationTop"> 
                        <li data-divtargetmenu="d1" class="optionsMenu"><a href="./whats_new.html">NUEVA COLECCIÓN</a></li>
                        <li data-divtargetmenu="d2" class="optionsMenu"><a href="#">DAMA</a></li>
                        <li data-divtargetmenu="d3" class="optionsMenu"><a href="#">CABALLERO</a></li>
                        <li data-divtargetmenu="d4" class="optionsMenu"><a href="#">BOLSAS</a></li>
                        <li data-divtargetmenu="d5" class="optionsMenu"><a href="#">ACCESORIOS</a></li>
                        <li data-divtargetmenu="d6" class="optionsMenu"><a href="#">PROMOCIONES</a></li>
                        <li data-divtargetmenu="d7" class="optionsMenu"><a href="#">CLEARANCE</a></li>
                        <li data-divtargetmenu="d8" class="optionsMenu"><a href="#">TENDENCIAS</a></li>
                        <li class="externalLinkMenu"><a href="http://www.stevemadden.com/world/world_landing.jsp?cm_sp=globalnav-_-smworld" target="_blank">SM WORLD</a></li>
                     </ul>
                  </div>
               </nav>
            </div>
            <!-- row --> 
         </div>
         <!-- container fluid -->  
  
        

        <!-- cada columna necesita tener un identificador count-** para la funcion de javascript --> 
         <div class="options-bar">  
            
            <div class="menuContent d1 black"> 
               <div class="count-d1 col-xs-6 rightBorder">
                  <img class="menuCircleImg" src="./images/content/e_1.png" alt=""> 
                  <ul class="listMenuTop">
                     <li><a href="#">WHAT'S NEW</a></li>
                     <li><a href="#">SALE </a> </li>
                     <li><a href="#">CLEARANCE </a> </li>
                     <li><a href="#">SHOP BY SIZE </a> </li> 
                  </ul>
               </div>
               <div class="count-d1 col-xs-6">
                  <img class="img-menu" src="./images/default/whats.gif" alt="">
               </div>
            </div>

            <div class="menuContent d2 black">
               <div class="count-d2 col-xs-3 rightBorder">
                  <img class="menuCircleImg" src="./images/content/e_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA COLEECIÓN</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                     <li><a href="#">COMPRA POR TALLA </a> </li>
                  </ul>
               </div>
               <div class="count-d2 col-xs-3 rightBorder">
                  <img class="menuCircleImg" src="./images/content/e_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">SANDALIAS</a></li>
                     <li><a href="#">FLATS</a></li>
                     <li><a href="#">STILLETOS</a></li>
                     <li><a href="#">PUMPS  </a> </li>
                     <li><a href="#">CUÑAS  </a> </li>
                     <li><a href="#">SNEAKERS</a> </li>
                     <li><a href="./catalog.html">BOTINES  </a> </li>
                     <li><a href="#">BOTAS </a></li>
                  </ul>
               </div>
               <div class="count-d2 col-xs-3 rightBorder">
                  <img class="menuCircleImg" src="./images/content/e_3.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">OPEN-TOE-BOTTIES </a></li>
                     <li><a href="#">CHUNKY HEELS </a></li>
                     <li><a href="#">TIRAS </a></li>
                     <li><a href="#">ALPARGATAS </a></li>
                     <li><a href="#">NUDES </a></li>
                     <li><a href="#">GRADUACIONES </a></li>
                     <li><a href="#">MADDEN GIRL </a></li>
                  </ul>
               </div> 
               <div class="count-d2 col-xs-3">
                  <img class="img-menu" src="./images/content/a_1.jpg"  alt="">
               </div>
            </div>

            <div class="menuContent d3 black">
               <div class="count-d3 col-xs-3 rightBorder">
                  <img class="menuCircleImg" src="./images/content/s_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA COLECCIÓN</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                     <li><a href="#">COMPRA POR TALLA </a> </li>
                  </ul>
               </div>
               <div class="count-d3 col-xs-3 rightBorder">
                  <img class="menuCircleImg" src="./images/content/s_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">CASUAL</a></li>
                     <li><a href="#">SNEAKERS</a></li>
                     <li><a href="#">VESTIR</a></li>
                     <li><a href="#">BOTAS  </a> </li>
                  </ul>
               </div>
               <div class="count-d3 col-xs-3 rightBorder"> 
                  <img class="menuCircleImg" src="./images/content/s_3.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">LACE-UP </a></li>
                     <li><a href="#">SLIP-ON </a></li>
                     <li><a href="#">CHUKKAS </a></li>
                  </ul>
               </div>
               <div class="count-d3 col-xs-3">
                  <img class="img-menu" src="./images/default/cool.jpg"  alt="">
               </div>
            </div>


<!-- 
            <div class="menuContent d4 black">
               <div class="count-d4 col-xs-6 rightBorder">
                  <img class="menuCircleImg" src="./images/content/s_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA COLECCIÓN</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                  </ul>
               </div>
               <div class="count-d4 col-xs-6">
                  <img class="menuCircleImg" src="./images/content/s_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">CASUAL</a></li>
                     <li><a href="#">SNEAKERS</a></li>
                     <li><a href="#">VESTIR</a></li>
                     <li><a href="#">BOTAS  </a> </li>
                  </ul>
               </div>
            </div>
-->  


            <div class="menuContent d5 black">
               <div class="count-d5 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA COLECCIÓN</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                  </ul>
               </div>
               <div class="count-d5 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">LAGWEAR</a></li>
                     <li><a href="#">LENTES</a></li>
                     <li><a href="#">JOYAS</a></li>
                     <li><a href="#">SOMBREROS Y SCARVES  </a> </li>
                     <li><a href="#">BOLSAS  </a> </li>
                  </ul>
               </div>
               <div class="count-d5 col-xs-4">
                  <img class="img-menu" src="./images/content/sun.gif"  alt="">
               </div>
            </div>

<!--
            <div class="menuContent d6 black">
               <div class="count-d6 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                  </ul>
               </div>
               <div class="count-d6 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">LAGWEAR</a></li>
                     <li><a href="#">LENTES</a></li>
                     <li><a href="#">JOYAS</a></li>
                     <li><a href="#">SOMBREROS Y SCARVES  </a> </li>
                  </ul>
               </div>
               <div class="count-d6 col-xs-4">
                  <img class="img-menu" src="./images/content/sun.gif"  alt="">
               </div>
            </div>
--> 



            <div class="menuContent d8 black"> 
               <div class="count-d8 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_1.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">NUEVA COLECCIÓN</a></li>
                     <li><a href="#">PROMOCIONES </a> </li>
                     <li><a href="#">SALE </a> </li>
                  </ul>
               </div>
               <div class="count-d8 col-xs-4 rightBorder">
                  <img class="menuCircleImg" src="./images/content/v_2.png"  alt="">
                  <ul class="listMenuTop">
                     <li><a href="#">LAGWEAR</a></li>
                     <li><a href="#">LENTES</a></li>
                     <li><a href="#">JOYAS</a></li>
                     <li><a href="#">SOMBREROS Y SCARVES  </a> </li>
                     <li><a href="#">BOLSAS  </a> </li>
                  </ul>
               </div>
               <div class="count-d8 col-xs-4">
                  <img class="img-menu" src="./images/content/sun.gif"  alt="">
               </div>
            </div>
         </div>
         <!-- options bar --> 
      </header>



