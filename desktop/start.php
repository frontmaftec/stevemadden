<?php include('./header.php');  ?>  

<div class="container-fluid marginQuery">
<div class="row" id="homeBanner">
   <div class="swiper-container sslide">
      <div class="swiper-wrapper">
         <div class="swiper-slide">
            <img class="img-responsive" src="./images/default/b_1.jpg" alt=""> 
         </div>
         <div class="swiper-slide">
            <img class="img-responsive" src="./images/default/b_1.jpg" alt=""> 
         </div>
      </div>
      <nav id="control_rd">
         <span class="fs-prev swiper-button-prev"></span> 
         <span class="fs-next swiper-button-next"></span>
      </nav>
   </div>
</div>
<!-- row --> 
<div class="row">
   <div class="col-xs-12">
      <img class="img-responsive" src="./images/default/b_2.jpg" alt=""> 
   </div>
</div>
<!-- row --> 
<div class="row" id="middleContentMargin">
   <div class="col-xs-4"> <img class="img-responsive" src="./images/default/b_3.jpg" alt=""> </div>
   <div class="col-xs-4">
      <h2 class="title_s1">SM'S PICKS</h2>
      <div class="h_mininav">
         <a href="#" class="comp navi-l swiper-button-prev"></a> 
         <a href="#" class="comp navi-r swiper-button-next"></a> 
      </div>
      <div class="swiper-container swhome">
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <div class="centre">
                  <div class="lista_productos">
                     <img class="SingleImageSwiper" src="./images/default/p_1.jpg" alt=""> 
                     <p class="name"><a class="h_image" href="#">FACTIONN</a></p>
                     <p class="price"><span class="fontLight18">$59.95</span></p>
                  </div>
               </div>
            </div>
            <div class="swiper-slide">
               <div class="centre">
                  <div class="lista_productos">
                     <img class="SingleImageSwiper" src="./images/default/p_1.jpg" alt=""> 
                     <p class="name"><a class="h_image" href="#">FACTIONN</a></p>
                     <p class="price"><span class="fontLight18">$59.95</span></p>
                  </div>
               </div>
            </div>
            <div class="swiper-slide">
               <div class="centre">
                  <div class="lista_productos">
                     <img class="SingleImageSwiper" src="./images/default/p_1.jpg" alt=""> 
                     <p class="name"><a class="h_image" href="#">FACTIONN</a></p>
                     <p class="price"><span class="fontLight18">$59.95</span></p>
                  </div>
               </div>
            </div>
            <div class="swiper-slide">
               <div class="centre">
                  <div class="lista_productos">
                     <img class="SingleImageSwiper" src="./images/default/p_1.jpg" alt=""> 
                     <p class="name"><a class="h_image" href="#">FACTIONN</a></p>
                     <p class="price"><span class="fontLight18">$59.95</span></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xs-4"> <img class="img-responsive" src="./images/content/g_1.gif" alt=""></div>
</div>
<!-- row -->  
</div> <!-- container-fluid marginQuery --> 
<?php include('./footer.php');  ?>