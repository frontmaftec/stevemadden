<?php include('./includes/header.php');  ?>  
<div class="container"> 
   <div class="row">  
      <div class="col-xs-12"> 
         <a class="infoLink" href="#"> Continue Shopping </a>
         <h1 class="shopping_bag">SHOPPING BAG</h1>
         <div class="about_cart normalFontOut">
            <p>Items will be saved in your Shopping Bag for 60 days, however, we cannot guarantee the inventory availability at that time. 
               <span style="font-weight:bold">Items in your shopping cart are not reserved until you place your order.</span> If you are signed in, your items may be viewed on your computer, tablet, or mobile phone.&nbsp;<a href="#" target="_blank">Need Help?</a>
            </p>
         </div>
      </div>
   </div>
   <hr class="solid">
   <div class="row">
      <div class="col-xs-4">
         <img class="img-responsive" src="./images/catalog/a_1.jpg" alt="#">
      </div>
      <div class="col-xs-6 sr">
         <div class="infoResumeCart">
            <h3>M-FRAY</h3>
            <p>COLOR: <span>BLACK MULTI</span></p>
            <p>SIZE: <span>ONESZ</span></p>
         </div>
         <div class="priceResumeCart">
            <p>2,300 - <span>QTY: 1</span></p>
         </div>
         <div class="actionsResumeCart">
            <p>In Stock</p>
            <a href="#">EDIT ITEM</a> - 
            <a href="#">REMOVE ITEM</a>
         </div>
      </div>
      <div class="calcPriceCart">
         <div class="col-xs-2 sr">2.232</div>
      </div>
   </div>
   <hr class="dotted">
   <div class="row">
      <div class="col-xs-4">
         <img class="img-responsive" src="./images/catalog/a_1.jpg" alt="#">
      </div>
      <div class="col-xs-6 sr">
         <div class="infoResumeCart">
            <h3>M-FRAY</h3>
            <p>COLOR: <span>BLACK MULTI</span></p>
            <p>SIZE: <span>ONESZ</span></p>
         </div>
         <div class="priceResumeCart">
            <p>2,300 - <span>QTY: 1</span></p>
         </div>
         <div class="actionsResumeCart">
            <p>In Stock</p>
            <a href="#">EDIT ITEM</a> - 
            <a href="#">REMOVE ITEM</a>
         </div>
      </div>
      <div class="calcPriceCart">
         <div class="col-xs-2 sr">2.232</div>
      </div>
   </div>
   <hr class="solid">
   <div class="row">
      <div class="col-xs-12">
         <div class="method">
            <h4>Shipping Method</h4>
            <select name="#" id="cat_value">
               <option value="#">DEFAULT</option>
               <option value="#">VALUE</option>
               <option value="#">VALUE</option>
               <option value="#">VALUE</option>
               <option value="#">VALUE</option>
               <option value="#">VALUE</option>
               <option value="#">VALUE</option>
            </select>
         </div>
      </div>
   </div>
   <div class="row totalPricesCombo">
      <div class="col-xs-6">
         <h4>	Merchandise:</h4>
      </div>
      <div class="col-xs-6">
         <h4 class="text-right">	523.00 MXN</h4>
      </div>
      <div class="col-xs-6">
         <h4>	<a href="#">Estimated Shipping:</a></h4>
      </div>
      <div class="col-xs-6">
         <h4 class="text-right">	52.00 MXN</h4>
      </div>
      <div class="col-xs-6">
         <h4>	Estimated Sales Tax:</h4>
      </div>
      <div class="col-xs-6">
         <h4 class="text-right">	613.00 MXN</h4>
      </div>
   </div>
   <hr class="dotted">
   <div class="row final_total">
      <div class="col-xs-6">
         <h2>	Merchandise:</h2>
      </div>
      <div class="col-xs-6">
         <h2 class="text-right">	523.00 MXN</h2>
      </div>
      <div class="col-xs-12">
         <p>Final shipping and tax will be calculated during checkout based on shipping addresses.</p>
      </div>
      <div class="col-xs-12">	 
         <input class="standarButton begin" id="" type="submit" value="BEGIN CHECKOUT" autofillparam="ON">
      </div>
   </div>
   <div class="payOptions">
      <img class="img-responsive" src="./images/default/payment_types.gif" alt=""> 
   </div>
   <?php include('./x_info.php'); ?>
</div>
<!-- container --> 
<?php include('./includes/footer.php');  ?>