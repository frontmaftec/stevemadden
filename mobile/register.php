<?php include('./includes/header.php');  ?>
<div class="container">
   <div class="row">
      <div class="col-xs-12">
         <h1>REGISTER YOUR STEVE MADDEN ACCOUNT</h1>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-12"> 

      <p class="fontLight18 common-error">Error.</p>
      <p class="fontLight18 common-success">Success.</p>   
      <p class="fontLight18 common-warning">Warning.</p>
      <p class="fontLight18 common-info">Info.</p>

         <form class="registerForm" action="#">
            <div class="form-group spaceRegister">
               <label for="male">NOMBRE* </label>
               <input class="txtStandar" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister">
               <label for="male">APELLIDO* </label>
               <input class="txtStandar" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister"> 
               <label for="male">E-MAIL* </label>
               <input class="txtStandar" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister">
               <label for="male">CONTRASEÑA* </label>
               <input class="txtStandar" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister">
               <label for="male">CONFIRMAR CONTRASEÑA* </label>
               <input class="txtStandar" name="#" id="#" type="password" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister">
               <label for="male">SEXO* </label>
               <input class="txtStandar" name="#" id="#" type="text" autocomplete="off" spellcheck="false">
            </div>
            <div class="form-group spaceRegister">
               <label>CUMPLEAÑOS:</label> 
               <div class="row">
                  <div class="col-xs-4 setOfDates">
                     <select class="cumpleSelect" name="dayBirthday" id="">
                        <option value="#">01</option>
                        <option value="#">02</option>
                     </select>
                  </div>
                  <div class="col-xs-4 setOfDates">
                     <select class="cumpleSelect" name="monthBirthday" id="">
                        <option value="#">01</option>
                        <option value="#">02</option>
                     </select>
                  </div>
                  <div class="col-xs-4 setOfDates">
                     <select class="cumpleSelect" name="yearBirthday" id="">
                        <option value="#">01</option>
                        <option value="#">02</option>
                     </select>
                  </div>
               </div>
            </div>


 <div class="selectNews">
    
<input style="margin-right: 5px;" id="nb1" value="nb1" name="nb1" type="checkbox">
<label for="nb1" class="#">Suscríbete A Nuestro  Newsletter</label>

 </div>
 

            <input class="standarButton w10" id="#" type="submit" value="Enviar">
         </form>
      </div>
      <!-- row --> 
   </div>
   <!-- row --> 
</div>

<?php include('./includes/footer.php');  ?>  