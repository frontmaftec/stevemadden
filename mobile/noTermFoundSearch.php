<?php include('./includes/header.php');  ?> 
<div class="container">
   <div class="row noResults">
      <div class="col-xs-12">
         <h2>SEARCH RESULTS</h2>
         <h2>WE´RE SORRY. WE WERE UNABLE TO FIND</h2>
         <h2>THATH MATCHED <span>TERM SEARCH</span></h2>
         <form action="#">
            <input class="boxNoResults" name="UserEmail" value="">
            <input class="btnNoResults" type="submit" value="GO">
         </form>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-12">
         <h2 class="otherResult">SEARCH HELP</h2>
         <p class="tryToSearch">Check your spelling. Only minor misspellings can be corrected.</p>
         <p class="tryToSearch">Check your spelling. Only minor misspellings can be corrected.</p>
         <h2 class="otherResult">OTHER SUGGESTIONS</h2>
         <p class="tryToSearch">
            <a href="#">SEE A LIST OF CATEGORIES</a>
         </p>
         <p class="tryToSearch">
            <a href="#">RETURN TO THE HOME PAGE</a>
         </p>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-12">
         <div class="boxMadden">
            <h2>1-888-SMADDEN</h2>
            <p class="normalFontOut">Need Help? Call our customer service. We´re happy to assist you 9am - 10pm ET, 7 days a week</p>
         </div>
      </div>
   </div>
</div>
<?php include('./includes/footer.php');  ?>