//Funcion utilizada para mostrar producto en single-product.php
$(document).ready(function() { 
 
   var swiper = new Swiper('.imageProductSlide', { 
       pagination: '.swiper-pagination',
       slidesPerView: 1,
       paginationClickable: true, 
       nextButton: '.swiper-button-next',
       prevButton: '.swiper-button-prev'
   
                                       }); 
                              });  

 


//Muestra y oculta el Menu y la caja de busqueda si esta abierta
$( ".controlMenu" ).click(function() { 
 
		$(".menuMobileTop").toggleClass( "hide" );
    $(".topSearchBlue").removeClass("open");  
    $(".controlSearch").removeClass( "m_close" );  

								                      });

 
//Muestra y oculta la caja de busqueda y el menu si esta abierto 
$( ".controlSearch" ).click(function() {  
 
    $(".controlSearch").toggleClass( "m_close" );    
    $(".topSearchBlue").toggleClass("open"); 
    $(".menuMobileTop").addClass( "hide" ); 

                                        }); 


 //Muestra opcion del menu 
function action_menu (recibe) {  $("."+recibe+"").toggleClass( "show" ); }



//Abre las opciones de filtro en catalogo.php
$( ".filterProduct" ).click(function() {
    
    $(".setFilters.controles").toggleClass("open");  

                                       });
