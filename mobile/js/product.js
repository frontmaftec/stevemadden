
// Funcion para collapse de opciones de producto remplaza al original de Bootstrap compatible Jquery 1.7.2

$(document).ready(function() { 

    $("#collapseInfo h3").removeClass();  //Limpia la clase 

    $("#collapseInfo h3").click(function() {   // Si le dan clic activa la funcion 

        $("#collapseInfo .comun_collapse").css("display", "none");  // Esconde todos los elementos

        var abierto = $(this).attr('class');  // Obtiene clase para conocer el status de ese collapse 

        $("#collapseInfo h3").removeClass();     // Limpia la clase para evitar confusion en el if 

        if (abierto != 'undefined') {    // Si no se encuentra le asigna un valor abierto 
            $(this).toggleClass('abierto');
            $(this).next(".comun_collapse").css("display", "block");
        }


        if (abierto == 'abierto') {   // Cuando le damos clic al mismo elemento oculta y despliega - oculta y despliega  
            $(this).toggleClass('abierto');
            $(this).next(".comun_collapse").css("display", "none");
        }


                                      });

    $("#first").trigger("click");   //Inicia el collapse 

                            });