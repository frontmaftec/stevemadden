<?php include('./includes/header.php');  ?> 
<div class="container">
   <div class="row">
      <div class="col-xs-12">      
         <a class="infoLink" href="#">BACK TO ---</a>
      </div>
      <div class="col-xs-6 titleProductDetail">
         <h1>BOOTIE</h1> 
         <h3>$1323.00</h3>
         <a class="infoLink" href="#">WRITE A REVIEW</a>
      </div>
      <div class="col-xs-6">
         <div class="badgeProduct new badgeDetailProduct"></div>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-12">
         <div class="swiper-container imageProductSlide">
            <div class="swiper-wrapper">
               <div class="swiper-slide">
                  <img class="img-responsive" src="./images/product/d_1.jpg" alt=""> 
               </div> 
               <div class="swiper-slide">
                  <img class="img-responsive" src="./images/product/d_2.jpg" alt=""> 
               </div>
               <div class="swiper-slide">
                  <img class="img-responsive" src="./images/product/d_3.jpg" alt=""> 
               </div>
            </div>
            <div class="swiper-pagination detail"></div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-xs-12">
         <div class="txt_prod">
            <p> COLOR: MULTI SNAKE - $3.699 MXN</p>
            <div class="mini_options"> 
               <a href="#"><img class="active" src="./images/default/opc_colors.jpg" alt=""></a>
               <a href="#"><img src="./images/default/opc_colors.jpg" alt=""></a>
            </div>
         </div>
      </div>
   </div>

   <div class="row">  
    <form action="./">  
      <div class="col-xs-6">
         <p class="titleFilter">SIZE</p>
         <select name="#" id="cat_value">
            <option value="#">DEFAULT</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
         </select>
      </div>
      <div class="col-xs-6">
         <p class="titleFilter">QTY</p>
         <select name="#" id="cat_value">
            <option value="#">DEFAULT</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
            <option value="#">VALUE</option>
         </select>
      </div>

      <div class="col-xs-12">
         <div class="add_cart">
            <input onclick="location.href='./cart.php';" class="standarButton w10" id="emailSignUpSubmit" type="submit" value="AGREGAR AL CARRITO">
      </div>
     </div> 
   </form> 
</diV> <!-- row --> 




<div class="row">   
   
  <div class="col-xs-6 brdLeft">
  <a href="#"> <span class="detailFavorite"></span> </a>
  </div>
  
  <div class="col-xs-6 txt-center">
  <a class="infoLink" href="#"> <span>Find in a Store</span> </a>
  </div>

</div>




         <div id="collapseInfo">
            <h3 id="first" class="abierto">Descripción</h3>
            <div class="comun_collapse  default" style="display: block;">
               <p>Chaqueta completamente bordada en canutillos dorados en todo el cuerpo y mangas.  Chaqueta con escote solapada y mangas larga. 
                  Forrada a tono. Tejido Premium. Prenda exclusiva
               </p>
            </div>
            <h3>Material</h3>
            <div class="comun_collapse " style="display: none;">
               <p>100% Poliéster-Polyester-Poliéster Con Aplicación/Com Aplicação Forro-Lining-Forro: 100% Viscosa-Viscose-Viscose</p>
            </div>
            <h3>Cuidado de la ropa</h3>
            <div class="comun_collapse " style="display: none;">
               <p>Lavado En Seco Con Percloroetileno En Tintorería Tradicional - No Exprimir - No Lavar - No Planchar - No Secar En Máquina - No Usar Blanqueador -</p>
            </div>
            <h3>Medios de Pago</h3>
            <div class="comun_collapse " style="display: none;">
               <div><img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/mercadopago.png" alt="MercadoPago"></div>
            </div>
            <h3>Métodos de Envío</h3>
            <div class="comun_collapse " style="display: none;">
               <p>Podrás elegir entre las siguientes opciones de envío:</p>
               <ul>
                  <li>Envío estándar: vía Correos.</li>
                  <li>Retiro en sucursal.</li>
                  <li>Pick up in store – Local Rapsodia: Local DOT o Local Recoleta Mall.</li>
               </ul>
               <p>Para más información consulta <a title="Envío" href="/envio">aquí</a></p>
            </div>
            <h3>Contacto</h3>
            <div class="comun_collapse " style="display: none;">
               <div>Atención telefónica: <a href="tel:3221-6869">3221-6869</a></div>
               <div>Escribinos a: <a href="mailto:contacto@steve.com">contacto@steve.com</a></div>
            </div>



         </div>          <?php include('./x_info.php'); ?>  
      </div>
   </div>
</div>
<?php include('./includes/footer.php');  ?>