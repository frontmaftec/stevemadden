
<footer> 

<div class="container">
  
<div class="row">   

<div class="col-xs-12"> 

<h4 class="signup">EMAIL SIGNUP</h4>
<h4 class="discount">Get 10% off when you sign up for emails</h4>

<form action="#">
<input class="boxUserMail" name="UserEmail" type="email" value="" size="20" maxlength="65">
<input class="standarButton" id="" type="submit" value="GO">
</form>



<p class="footerNormalFont">Steven Madden, Ltd requires that our customers comply with The Children’s Online Privacy Protection Act which prohibits the collection of any information from children under the age of 13.</p>
 
 

<ul class="footerLinks">
   <li>
      <a href="#" title="#">
      <span>About Us</span>
      </a>
   </li>
   <li>
      <a href="#" title="#">
      <span>Privacy Policy</span>
      </a>
   </li>
   <li class="last">
      <a href="#" title="#">
      <span>Sign Out</span>
      </a>
   </li>
   <br>
   <li><a href="#" title="#"><span>Corporate Opportunities</span></a></li>
   <li class="last"><a href="#" title="#" target="_blank"><span>Retail Opportunities</span></a></li>
</ul>

 
<ul class="socialIconsBlack">
            <a href="https://www.facebook.com/SteveMaddenMx/" title="Facebook" target="_blank"><li class="facebook"> <span>Facebook</span></li></a>
            <a href="https://twitter.com/SteveMadden" title="Twitter" target="_blank"><li class="twitter"><span>Twitter</span></li></a>
            <a href="https://www.instagram.com/stevemaddenmx/" title="Instagram" target="_blank"><li class="instagram"><span>Instagram</span></li></a>
</ul>



<div class="footerNormalFont text-center"> 2013© Steven Madden, LTD. All Rights Reserved </div>


   </div>

</div>

</div>
 
</footer>

<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>  
<script src="./js/swiper.js"></script>   

<script src="./js/home.js"></script>  
<script src="./js/product.js"></script>  

</body>
</html>
